/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 6              */
/* Grafika rastrowa                                          J. Tarasiuk 2008 */
/******************************************************************************/

#include <wx/scrolwin.h>
#include "Main_Frm.h"
#include "vecmat.h"
#include "math.h"
#include <wx/dcbuffer.h>

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End
#define wxT

//----------------------------------------------------------------------------
// Main_Frm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(Main_Frm, wxFrame)
                ////Manual Code Start
                ////Manual Code End

                EVT_CLOSE(Main_Frm::OnClose)
								EVT_BUTTON(ID_WXBUTTON11, Main_Frm::WxButton11Click)
                EVT_BUTTON(ID_WXBUTTON10, Main_Frm::WxButton10Click)
                EVT_BUTTON(ID_WXBUTTON9, Main_Frm::WxButton9Click)

                EVT_COMMAND_SCROLL(ID_WXSCROLLBAR2, Main_Frm::WxScrollBar2Scroll)

                EVT_COMMAND_SCROLL(ID_WXSCROLLBAR1, Main_Frm::WxScrollBar1Scroll)
                EVT_BUTTON(ID_WXBUTTON8, Main_Frm::WxButton8Click)
                EVT_BUTTON(ID_WXBUTTON7, Main_Frm::WxButton7Click)
                EVT_BUTTON(ID_WXBUTTON6, Main_Frm::WxButton6Click)
                EVT_BUTTON(ID_WXBUTTON5, Main_Frm::WxButton5Click)
                EVT_BUTTON(ID_WXBUTTON4, Main_Frm::WxButton4Click)
                EVT_BUTTON(ID_WXBUTTON3, Main_Frm::WxButton3Click)
                EVT_BUTTON(ID_WXBUTTON2, Main_Frm::WxButton2Click)
                EVT_BUTTON(ID_WXBUTTON1, Main_Frm::WxButton1Click)

                EVT_UPDATE_UI(ID_WXSCROLLEDWINDOW1, Main_Frm::WxScrolledWindow1UpdateUI)
END_EVENT_TABLE()
////Event Table End

wxString Float2String(float f, int numdigits) {
    wxString Result;
    if (numdigits == 0) {
        Result = Result.Format("%f", f);
    } else {
        wxString strNumFormat = wxString::Format("%%0.%df", numdigits);
        Result = Result.Format(strNumFormat, f);
    }
    return Result;
}

Main_Frm::Main_Frm(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize &size,
                   long style)
        : wxFrame(parent, id, title, position, size, style) {
    CreateGUIControls();
    this->SetBackgroundColour(wxColour(192, 192, 192));
    wxImage::AddHandler(new wxJPEGHandler);       // Dodajemy handlery do formatow
    wxImage::AddHandler(new wxPNGHandler);        // z jakich bedziemy korzytsac
    wxImage image("Output/test.jpg");                      // Tworzymy image wczytujac od razu obrazk z pliku
    if (!image.Ok()) wxMessageBox("Nie udało się załadowac obrazka");
    Img_Org = image.Copy();                           // Kopiujemy obrazek do Img_Org
    Img_Cpy = Img_Org.Copy();                         // Kopiujemy obrazek do Img_Cpy
    image.LoadFile("Output/mask.png");                     // Wczytujemy obrazek bedacy maska
    if (!image.Ok()) wxMessageBox("Nie udało się załadowac maski");
    Img_Mask = image.Copy();                          // Kopiujemy maske do Img_Mask
    WxScrolledWindow1->SetScrollbars(25, 25, 52, 40);  // Ustalamy zakres zmiennosci suwakow okna z suwakami
    WxScrolledWindow1->SetBackgroundColour(wxColour(192, 192, 192));
}

Main_Frm::~Main_Frm() {
}

void Main_Frm::CreateGUIControls() {
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    WxBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    this->SetSizer(WxBoxSizer1);
    this->SetAutoLayout(true);

    WxScrolledWindow1 = new wxScrolledWindow(this, ID_WXSCROLLEDWINDOW1, wxPoint(5, 5), wxSize(640, 480),
                                             wxVSCROLL | wxHSCROLL);
    WxScrolledWindow1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer1->Add(WxScrolledWindow1, 1, wxEXPAND | wxALL, 5);

    WxGridSizer1 = new wxGridSizer(0, 1, 0, 0);
    WxBoxSizer1->Add(WxGridSizer1, 0, wxALIGN_CENTER | wxALL, 5);

    WxGridSizer2 = new wxGridSizer(0, 2, 0, 0);
    WxGridSizer1->Add(WxGridSizer2, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton1 = new wxButton(this, ID_WXBUTTON1, wxT("Grayscale"), wxPoint(5, 5), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton1"));
    WxButton1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton1, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton2 = new wxButton(this, ID_WXBUTTON2, wxT("Blur"), wxPoint(90, 5), wxSize(75, 25), 0, wxDefaultValidator, wxT
                             ("WxButton2"));
    WxButton2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton2, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton3 = new wxButton(this, ID_WXBUTTON3, wxT("Mirror"), wxPoint(5, 40), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton3"));
    WxButton3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton3, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton4 = new wxButton(this, ID_WXBUTTON4, wxT("Replace"), wxPoint(90, 40), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton4"));
    WxButton4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton4, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton5 = new wxButton(this, ID_WXBUTTON5, wxT("Rescale"), wxPoint(5, 75), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton5"));
    WxButton5->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton5, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton6 = new wxButton(this, ID_WXBUTTON6, wxT("Rotate"), wxPoint(90, 75), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton6"));
    WxButton6->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton6, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton7 = new wxButton(this, ID_WXBUTTON7, wxT("RotateHue"), wxPoint(5, 110), wxSize(75, 25), 0,
                             wxDefaultValidator, wxT("WxButton7"));
    WxButton7->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton7, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton8 = new wxButton(this, ID_WXBUTTON8, wxT("Mask"), wxPoint(90, 110), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton8"));
    WxButton8->SetHelpText(wxT("SetMaskFromImage"));
    WxButton8->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxGridSizer2->Add(WxButton8, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    WxGridSizer1->Add(WxBoxSizer2, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText1 = new wxStaticText(this, ID_WXSTATICTEXT1, wxT("Jasnośc"), wxPoint(64, 5), wxDefaultSize, 0, wxT
                                     ("WxStaticText1"));
    WxStaticText1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxStaticText1, 0, wxALIGN_CENTER | wxALL, 5);

    WxScrollBar1 = new wxScrollBar(this, ID_WXSCROLLBAR1, wxPoint(5, 32), wxSize(160, 16), wxSB_HORIZONTAL,
                                   wxDefaultValidator, wxT("WxScrollBar1"));
    WxScrollBar1->Enable(false);
    WxScrollBar1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxScrollBar1, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText2 = new wxStaticText(this, ID_WXSTATICTEXT2, wxT("Kontrast"), wxPoint(62, 58), wxDefaultSize, 0, wxT
                                     ("WxStaticText2"));
    WxStaticText2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxStaticText2, 0, wxALIGN_CENTER | wxALL, 5);

    WxScrollBar2 = new wxScrollBar(this, ID_WXSCROLLBAR2, wxPoint(5, 85), wxSize(160, 16), wxSB_HORIZONTAL,
                                   wxDefaultValidator, wxT("WxScrollBar2"));
    WxScrollBar2->Enable(false);
    WxScrollBar2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxScrollBar2, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer3 = new wxBoxSizer(wxVERTICAL);
    WxGridSizer1->Add(WxBoxSizer3, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton9 = new wxButton(this, ID_WXBUTTON9, wxT("Maska Prewitta"), wxPoint(5, 5), wxSize(150, 25), 0,
                             wxDefaultValidator, wxT("WxButton9"));
    WxButton9->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxButton9, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton10 = new wxButton(this, ID_WXBUTTON10, wxT("Prog 128"), wxPoint(5, 40), wxSize(150, 25), 0,
                              wxDefaultValidator, wxT("WxButton10"));
    WxButton10->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxButton10, 0, wxALIGN_CENTER | wxALL, 5);

		WxButton11 = new wxButton(this, ID_WXBUTTON11, wxT("sin"), wxPoint(5, 70), wxSize(150, 25), 0, wxDefaultValidator, wxT("WxButton11"));
    WxButton11->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxButton11, 0, wxALIGN_CENTER | wxALL, 5);

    SetTitle(wxT("Lab_06"));
    SetIcon(wxNullIcon);

    GetSizer()->Layout();
    GetSizer()->Fit(this);
    GetSizer()->SetSizeHints(this);
    Center();

    ////GUI Items Creation End
    WxScrollBar1->Enable(true);
    WxScrollBar1->SetScrollbar(100, 1, 200, 10);
    WxScrollBar2->Enable(true);
    WxScrollBar2->SetScrollbar(100, 1, 200, 10);
}

void Main_Frm::OnClose(wxCloseEvent &event) {
    Destroy();
}

/*
 * WxScrolledWindow1UpdateUI
 */
void Main_Frm::WxScrolledWindow1UpdateUI(wxUpdateUIEvent &event) {
    wxBitmap bitmap(Img_Cpy);           // Tworzymy tymczasowa bitmape na podstawie Img_Cpy
    wxClientDC dc1(WxScrolledWindow1);   // Pobieramy kontekst okna
		wxBufferedDC dc(&dc1);
    dc.Clear();
    WxScrolledWindow1->DoPrepareDC(dc); // Musimy wywolac w przypadku wxScrolledWindow, zeby suwaki prawidlowo dzialaly
    dc.DrawBitmap(bitmap, 0, 0, true);  // Rysujemy bitmape na kontekscie urzadzenia
}

/*
 * WxButton1Click
 */
void Main_Frm::WxButton1Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy().ConvertToGreyscale();
}

/*
 * WxButton2Click
 */
void Main_Frm::WxButton2Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy().Blur(3);
}

/*
 * WxButton3Click
 */
void Main_Frm::WxButton3Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy().Mirror();
}

/*
 * WxButton4Click
 */
void Main_Frm::WxButton4Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy();
    Img_Cpy.Replace(254, 0, 0, 0, 0, 255);
}

/*
 * WxButton5Click
 */
void Main_Frm::WxButton5Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy().Rescale(320, 240);
}

/*
 * WxButton6Click
 */
void Main_Frm::WxButton6Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Rotate(M_PI * 30 / 180, wxPoint(Img_Org.GetWidth(), Img_Org.GetHeight()));
}

/*
 * WxButton7Click
 */
void Main_Frm::WxButton7Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy();
    Img_Cpy.RotateHue(0.5);
}

/*
 * WxButton8Click
 */
void Main_Frm::WxButton8Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy();
    Img_Cpy.SetMaskFromImage(Img_Mask, 0, 0, 0);
}

/*
 * WxScrollBar1Scroll
 */
void Main_Frm::WxScrollBar1Scroll(wxScrollEvent &event) {
// Tutaj, w reakcji na zmiane polozenia suwaka, wywolywana jest funkcja
// Brightness(...), ktora zmienia jasnosc. W tym miejscu nic nie
// zmieniamy. Do uzupelnienia pozostaje funkcja Brightness(...)
    wxEventType eventType = event.GetEventType();
    //if (eventType==10115)
    Brightness(WxScrollBar1->GetThumbPosition() - 100);
}

void Main_Frm::Brightness(int value) {
    Img_Cpy = Img_Org.Copy();
    unsigned char *RGBData = Img_Cpy.GetData();
    int RGBDataVolume = Img_Cpy.GetWidth() * Img_Cpy.GetHeight() * 3;
    for (int i = 0; i < RGBDataVolume; i++) {
        int tmpColourValue = RGBData[i] + value;
        tmpColourValue = tmpColourValue > 255 ? 255 : tmpColourValue < 0 ? 0 : tmpColourValue;
        RGBData[i] = static_cast<unsigned char>(tmpColourValue);
    }
}

/*
 * WxScrollBar2Scroll
 */
void Main_Frm::WxScrollBar2Scroll(wxScrollEvent &event) {
// Tutaj, w reakcji na zmiane polozenia suwaka, wywolywana jest funkcja
// Contrast(...), ktora zmienia kontrast. W tym miejscu nic nie
// zmieniamy. Do uzupelnienia pozostaje funkcja Contrast(...)
    wxEventType eventType = event.GetEventType();
    //if (eventType==10115)
    Contrast(WxScrollBar2->GetThumbPosition() - 100);
}

void Main_Frm::Contrast(int value) {
    Img_Cpy = Img_Org.Copy();
    unsigned char *RGBData = Img_Cpy.GetData();
    int RGBDataVolume = Img_Cpy.GetWidth() * Img_Cpy.GetHeight() * 3;
    float contrastParam = static_cast<float>(value + 100) / (100 - value);
    for (int i = 0; i < RGBDataVolume; i++) {
        int tmpColourValue = static_cast<int>((RGBData[i] - 128) * contrastParam + 128);
        tmpColourValue = tmpColourValue > 255 ? 255 : tmpColourValue < 0 ? 0 : tmpColourValue;
        RGBData[i] = static_cast<unsigned char>(tmpColourValue);
    }
}

/*
 * WxButton9Click
 */
void Main_Frm::WxButton9Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy();
    Matrix filter(1, 0, -1, 1, 0, -1, 1, 0, -1); // Prewitt

    unsigned char *RGBData = Img_Cpy.GetData();
    int RGBDataVol = Img_Cpy.GetWidth() * Img_Cpy.GetHeight() * 3;
    unsigned char *RGBDataCpy = new unsigned char[RGBDataVol];
    memcpy(RGBDataCpy, RGBData, sizeof(*RGBData) * RGBDataVol);
    for (int w = 1; w < Img_Cpy.GetWidth() - 1; w++) {
        for (int h = 1; h < Img_Cpy.GetHeight() - 1; h++) {
            Matrix red, green, blue;
            red[0][0] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w - 1) + 0];
            red[0][1] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w) + 0];
            red[0][2] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w + 1) + 0];
            red[1][0] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w - 1) + 0];
            red[1][1] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w) + 0];
            red[1][2] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w + 1) + 0];
            red[2][0] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w - 1) + 0];
            red[2][1] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w) + 0];
            red[2][2] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w + 1) + 0];

            green[0][0] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w - 1) + 1];
            green[0][1] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w) + 1];
            green[0][2] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w + 1) + 1];
            green[1][0] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w - 1) + 1];
            green[1][1] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w) + 1];
            green[1][2] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w + 1) + 1];
            green[2][0] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w - 1) + 1];
            green[2][1] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w) + 1];
            green[2][2] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w + 1) + 1];

            blue[0][0] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w - 1) + 2];
            blue[0][1] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w) + 2];
            blue[0][2] = RGBDataCpy[3 * ((h - 1) * Img_Cpy.GetWidth() + w + 1) + 2];
            blue[1][0] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w - 1) + 2];
            blue[1][1] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w) + 2];
            blue[1][2] = RGBDataCpy[3 * ((h) * Img_Cpy.GetWidth() + w + 1) + 2];
            blue[2][0] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w - 1) + 2];
            blue[2][1] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w) + 2];
            blue[2][2] = RGBDataCpy[3 * ((h + 1) * Img_Cpy.GetWidth() + w + 1) + 2];

            RGBData[3 * ((h) * Img_Cpy.GetWidth() + w) + 0] = static_cast<unsigned char>(red.applyFilter(filter) + 128 >
                                                                                         255 ? 255 :
                                                                                         red.applyFilter(filter) + 128 <
                                                                                         0 ? 0 :
                                                                                         red.applyFilter(filter) + 128);
            RGBData[3 * ((h) * Img_Cpy.GetWidth() + w) + 1] = static_cast<unsigned char>(
                    green.applyFilter(filter) + 128 > 255 ? 255 : green.applyFilter(filter) + 128 < 0 ? 0 :
                                                                  green.applyFilter(filter) + 128);
            RGBData[3 * ((h) * Img_Cpy.GetWidth() + w) + 2] = static_cast<unsigned char>(
                    blue.applyFilter(filter) + 128 > 255 ? 255 : blue.applyFilter(filter) + 128 < 0 ? 0 :
                                                                 blue.applyFilter(filter) + 128);
        }
    }
    delete[] RGBDataCpy;
};

/*
 * WxButton10Click
 */
void Main_Frm::WxButton10Click(wxCommandEvent &event) {
    Img_Cpy = Img_Org.Copy();
    unsigned char *RGBData = Img_Cpy.GetData();
    int RGBDataVol = Img_Cpy.GetHeight() * Img_Cpy.GetWidth() * 3;
    for (int i = 0; i < RGBDataVol; i++) {
        if (RGBData[i] < 128) RGBData[i] = 0;
        else RGBData[i] = 255;
    }

}

void Main_Frm::WxButton11Click(wxCommandEvent &event){
    Img_Cpy = Img_Org.Copy();
    for (int w = 0; w < Img_Cpy.GetWidth(); w++) {
        for(int h = 0; h < Img_Cpy.GetHeight(); h++){
        double r = Img_Cpy.GetRed(w, h)/255.0;
        double g = Img_Cpy.GetGreen(w, h)/255.0;
        double b = Img_Cpy.GetBlue(w, h)/255.0;
        double R = ((1+sin(r))/2.0)*255;
        double G = ((1+cos(r+g+b))/2.0)*255;
        double B = ((1+sin(b-r))/2.0)*255;	
        Img_Cpy.SetRGB(w, h, (unsigned char)R, (unsigned char)G, (unsigned char)B);
        }
    }

}
