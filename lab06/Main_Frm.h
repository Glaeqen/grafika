/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 6              */
/* Grafika rastrowa                                          J. Tarasiuk 2008 */
/******************************************************************************/

#ifndef __MAIN_FRM_h__
#define __MAIN_FRM_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/scrolbar.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/scrolwin.h>
#include <wx/sizer.h>
////Header Include End

////Dialog Style Start
#undef Main_Frm_STYLE
#define Main_Frm_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class Main_Frm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		Main_Frm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Lab_06"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = Main_Frm_STYLE);
		virtual ~Main_Frm();
		void WxScrolledWindow1UpdateUI(wxUpdateUIEvent& event);
		void WxButton1Click(wxCommandEvent& event);
		void WxButton2Click(wxCommandEvent& event);
		void WxButton3Click(wxCommandEvent& event);
		void WxButton4Click(wxCommandEvent& event);
		void WxButton5Click(wxCommandEvent& event);
		void WxButton6Click(wxCommandEvent& event);
		void WxButton7Click(wxCommandEvent& event);
		void WxButton8Click(wxCommandEvent& event);
		void WxScrollBar1Scroll(wxScrollEvent& event);
		void WxScrollBar2Scroll(wxScrollEvent& event);
		void WxButton9Click(wxCommandEvent& event);
		void WxButton10Click(wxCommandEvent& event);
		void WxButton11Click(wxCommandEvent& event);

		
	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxButton *WxButton11;

		wxButton *WxButton10;
		wxButton *WxButton9;
		wxBoxSizer *WxBoxSizer3;
		wxScrollBar *WxScrollBar2;
		wxStaticText *WxStaticText2;
		wxScrollBar *WxScrollBar1;
		wxStaticText *WxStaticText1;
		wxBoxSizer *WxBoxSizer2;
		wxButton *WxButton8;
		wxButton *WxButton7;
		wxButton *WxButton6;
		wxButton *WxButton5;
		wxButton *WxButton4;
		wxButton *WxButton3;
		wxButton *WxButton2;
		wxButton *WxButton1;
		wxGridSizer *WxGridSizer2;
		wxGridSizer *WxGridSizer1;
		wxScrolledWindow *WxScrolledWindow1;
		wxBoxSizer *WxBoxSizer1;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_WXBUTTON11 = 1100,
			ID_WXBUTTON10 = 1025,
			ID_WXBUTTON9 = 1024,
			ID_WXSCROLLBAR2 = 1022,
			ID_WXSTATICTEXT2 = 1021,
			ID_WXSCROLLBAR1 = 1020,
			ID_WXSTATICTEXT1 = 1019,
			ID_WXBUTTON8 = 1014,
			ID_WXBUTTON7 = 1013,
			ID_WXBUTTON6 = 1012,
			ID_WXBUTTON5 = 1011,
			ID_WXBUTTON4 = 1010,
			ID_WXBUTTON3 = 1009,
			ID_WXBUTTON2 = 1008,
			ID_WXBUTTON1 = 1007,
			ID_WXSCROLLEDWINDOW1 = 1002,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
		
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
	private:
        wxImage Img_Org,    // tu bedzie przechowywany oryginalny obrazek
                Img_Cpy,    // wszystkie zmiany beda wykonywane na tej kopii obrazka
                Img_Mask;   // tu bedzie przechowywana maska
        void Brightness(int value);     // funkcja zmieniajaca jasnosc obrazka
        void Contrast(int value);       // funkcja zmieniajaca kontrast obrazka
};

#endif
