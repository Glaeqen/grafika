#pragma once
#include <stdio.h>
#include <wx/gdicmn.h>


class Matrix {
    int data[3][3];
public:

    Matrix(int a = 1, int b = 0, int c = 0, int d = 0, int e = 1, int f = 0, int g = 0, int h = 0, int i = 1);

    int *operator[](unsigned index);

    const int *operator[](unsigned index) const;

    int applyFilter(const Matrix& filter);
};

