/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 6              */
/* Grafika rastrowa                                          J. Tarasiuk 2008 */
/******************************************************************************/

#include "Lab_06App.h"
#include "Main_Frm.h"

IMPLEMENT_APP(Main_FrmApp)

bool Main_FrmApp::OnInit()
{
    Main_Frm* frame = new Main_Frm(NULL);
    SetTopWindow(frame);
    frame->Show();
    return true;
}

int Main_FrmApp::OnExit()
{
	return 0;
}
