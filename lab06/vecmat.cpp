//
// Created by glaeqen on 31/03/17.
//
#include "vecmat.h"


Matrix::Matrix(int a, int b, int c, int d, int e, int f, int g, int h, int i) {
    data[0][0] = a;
    data[0][1] = b;
    data[0][2] = c;
    data[1][0] = d;
    data[1][1] = e;
    data[1][2] = f;
    data[2][0] = g;
    data[2][1] = h;
    data[2][2] = i;
}

int *Matrix::operator[](unsigned index) {
    return data[index];
}

const int *Matrix::operator[](unsigned index) const {
    return data[index];
}

int Matrix::applyFilter(const Matrix &filter) {
    int firstRow = filter[0][0] * data[0][0] + filter[0][1] * data[0][1] + filter[0][2] * data[0][2];
    int secondRow = filter[1][0] * data[1][0] + filter[1][1] * data[1][1] + filter[1][2] * data[1][2];
    int thirdRow = filter[2][0] * data[2][0] + filter[2][1] * data[2][1] + filter[2][2] * data[2][2];
    return - firstRow - secondRow - thirdRow;
}
