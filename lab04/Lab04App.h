//---------------------------------------------------------------------------
//
// Name:        Lab04App.h
// Author:      Janusz Malinowski
// Created:     2008-08-19 11:49:34
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __LAB04DLGApp_h__
#define __LAB04DLGApp_h__

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP

#include <wx/wx.h>

#else
#include <wx/wxprec.h>
#endif

class Lab04DlgApp : public wxApp {
public:
    bool OnInit();

    int OnExit();
};

#endif
