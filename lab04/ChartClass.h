#ifndef CHARTCLASS

#include "ConfigClass.h"
#include "vecmat.h"

class ChartClass {
private:
    ConfigClass *cfg;
    int screenWidth, screenHeight;
    int x_step;         // liczba odcinkow z jakich bedzie sie skladal wykres
    double x_min, x_max; // zakres zmiennej x
    double y_min, y_max; // zakres wartosci przyjmowanych przez funkcje
    double GetFunctionValue(double x); // zwraca wartosci rysowanej funkcji
    Matrix getTranslation(double offsetX, double offsetY) const;
    Matrix getRotation(double angle) const; // in degrees
    void rotateTheLine(wxRealPoint& start, wxRealPoint& end); // Rotates start and end accordingly

    void transformLine2D(Matrix transformation, wxRealPoint &start, wxRealPoint &end) const;

    wxPoint toScreenPoint(const wxRealPoint& realPoint) const;

    void drawAxis(wxDC *dc) ;

public:
    ChartClass(ConfigClass *c, int screenWidth = -1, int screenHeight = -1);

    void Set_Range();   // ustala wartosci zmiennych x_min,y_min,x_max,y_max
    double Get_Y_min() ; // zwraca y_min
    double Get_Y_max() ; // zwraca y_max
    void Draw(wxDC *dc) ;  // rysuje wykres
};

#define CHARTCLASS
#endif
