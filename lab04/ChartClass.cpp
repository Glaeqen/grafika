#include <cmath>

#include "ChartClass.h"

ChartClass::ChartClass(ConfigClass *c, int screenWidth, int screenHeight) {
    cfg = c;
    ChartClass::screenWidth = screenWidth;
    ChartClass::screenHeight = screenHeight;
    x_step = 2000;
    Set_Range();
}

void ChartClass::Set_Range() {
    double xmin = 9999.9, xmax = -9999.9, ymin = 9999.9, ymax = -9999.9;
    double x, y, step;
    int i;

    xmin = cfg->Get_x_start();
    xmax = cfg->Get_x_stop();

    step = (cfg->Get_x_stop() - cfg->Get_x_start()) / (double) x_step;
    x = cfg->Get_x_start();

    for (i = 0; i <= x_step; i++) {
        y = GetFunctionValue(x);
        if (y > ymax) ymax = y;
        if (y < ymin) ymin = y;
        x = x + step;
    }

    y_min = ymin;
    y_max = ymax;
    x_min = xmin;
    x_max = xmax;
}


double ChartClass::GetFunctionValue(double x) {
    if (cfg->Get_F_type() == 1) return x * x;
    if (cfg->Get_F_type() == 2) return 0.5 * exp(4 * x - 3 * x * x);
    return x + sin(x * 4.0);
}

double ChartClass::Get_Y_min() {
    Set_Range();
    return y_min;
}

double ChartClass::Get_Y_max() {
    Set_Range();
    return y_max;
}

Matrix ChartClass::getTranslation(double offsetX, double offsetY) const {
    Matrix matrix;
    matrix.data[0][0] = 1.0;
    matrix.data[1][1] = 1.0;
    matrix.data[0][2] = offsetX;
    matrix.data[1][2] = offsetY;
    return matrix;
}

Matrix ChartClass::getRotation(double angle /*degrees*/) const {
    double angleRadians = angle * M_PI / 180;
    Matrix matrix;
    matrix.data[0][0] = cos(angleRadians);
    matrix.data[0][1] = sin(angleRadians);
    matrix.data[1][0] = -sin(angleRadians);
    matrix.data[1][1] = cos(angleRadians);
    return matrix;
}

void ChartClass::rotateTheLine(wxRealPoint &start, wxRealPoint &end) {
    if (cfg->RotateScreenCenter()) {
        transformLine2D(getTranslation(-(cfg->Get_x0() + cfg->Get_x1()) / 2, -(cfg->Get_y0() + cfg->Get_y1()) / 2),
                        start,
                        end);
        transformLine2D(getRotation(cfg->Get_Alpha()), start, end);
        transformLine2D(getTranslation((cfg->Get_x0() + cfg->Get_x1()) / 2, (cfg->Get_y0() + cfg->Get_y1()) / 2),
                        start,
                        end);
    } else {
        transformLine2D(getRotation(cfg->Get_Alpha()), start, end);
    }
}

void ChartClass::transformLine2D(Matrix transformation, wxRealPoint &start, wxRealPoint &end) const {
    Vector startingPoint(start.x, start.y);
    Vector endingPoint(end.x, end.y);

    Vector afterTransformationStartingPoint = transformation * startingPoint;
    Vector afterTransformationEndingPoint = transformation * endingPoint;

    start.x = afterTransformationStartingPoint.data[0];
    start.y = afterTransformationStartingPoint.data[1];

    end.x = afterTransformationEndingPoint.data[0];
    end.y = afterTransformationEndingPoint.data[1];
}

wxPoint ChartClass::toScreenPoint(const wxRealPoint &realPoint) const {
    wxPoint pointOnScreen;
    pointOnScreen.x = static_cast<int>((realPoint.x - cfg->Get_x0()) * screenWidth / (cfg->Get_x1() - cfg->Get_x0()));
    pointOnScreen.y = static_cast<int>((1 - (realPoint.y - cfg->Get_y0()) / (cfg->Get_y1() - cfg->Get_y0())) *
                                       screenHeight);
    return pointOnScreen;
}

void ChartClass::drawAxis(wxDC *dc) {
    double independentXvalue = (cfg->Get_x1()-cfg->Get_x0())/70;
    double independentYvalue = (cfg->Get_y1()-cfg->Get_y0())/70;

    // X axis
    wxRealPoint startingXAxis(x_min, 0);
    wxRealPoint endingXAxis(x_max, 0);

    rotateTheLine(startingXAxis, endingXAxis);

    dc->DrawLine(toScreenPoint(startingXAxis), toScreenPoint(endingXAxis));

    // Arrow X
    wxRealPoint arrowXStart(x_max-independentXvalue, independentYvalue);
    wxRealPoint arrowXMiddle1(x_max, 0);
    wxRealPoint arrowXMiddle2(x_max, 0);
    wxRealPoint arrowXEnd(x_max-independentXvalue, -independentYvalue);

    rotateTheLine(arrowXStart, arrowXMiddle1);
    rotateTheLine(arrowXMiddle2, arrowXEnd);

    dc->DrawLine(toScreenPoint(arrowXStart), toScreenPoint(arrowXMiddle1));
    dc->DrawLine(toScreenPoint(arrowXMiddle2), toScreenPoint(arrowXEnd));

    // Delimiter on axis X
    for (int i = 0; x_min + i <= x_max; i++) {
        double pos = floor(x_min + i);
        wxRealPoint startingXDelimiter(pos, independentYvalue);
        wxRealPoint endingXDelimiter(pos, -independentYvalue);

        rotateTheLine(startingXDelimiter, endingXDelimiter);

        dc->DrawLine(toScreenPoint(startingXDelimiter), toScreenPoint(endingXDelimiter));

        wxRealPoint textCoord(pos-0.5*independentXvalue, -2*independentYvalue), blank(0, 0);
        wxString posText;
        posText.Printf("%0.0f", pos);

        rotateTheLine(textCoord, blank);

        dc->SetTextForeground(*wxBLACK);
        dc->DrawRotatedText(posText, toScreenPoint(textCoord), -cfg->Get_Alpha());
    }


    // Y axis

    wxRealPoint startingYAxis(0, y_min);
    wxRealPoint endingYAxis(0, y_max);

    rotateTheLine(startingYAxis, endingYAxis);

    dc->DrawLine(toScreenPoint(startingYAxis), toScreenPoint(endingYAxis));

    // Arrow X
    wxRealPoint arrowYStart(independentXvalue, y_max-independentYvalue);
    wxRealPoint arrowYMiddle1(0, y_max);
    wxRealPoint arrowYMiddle2(0, y_max);
    wxRealPoint arrowYEnd(-independentXvalue, y_max-independentYvalue);

    rotateTheLine(arrowYStart, arrowYMiddle1);
    rotateTheLine(arrowYMiddle2, arrowYEnd);

    dc->DrawLine(toScreenPoint(arrowYStart), toScreenPoint(arrowYMiddle1));
    dc->DrawLine(toScreenPoint(arrowYMiddle2), toScreenPoint(arrowYEnd));

    // Delimiter on axis Y
    for (int i = 0; y_min + i <= y_max; i++) {
        double pos = floor(y_min + i);
        wxRealPoint startingXDelimiter(independentXvalue, pos);
        wxRealPoint endingXDelimiter(-independentXvalue, pos);

        rotateTheLine(startingXDelimiter, endingXDelimiter);

        dc->DrawLine(toScreenPoint(startingXDelimiter), toScreenPoint(endingXDelimiter));

        wxRealPoint textCoord(2*independentXvalue, pos+1*independentYvalue), blank(0, 0);
        wxString posText;
        posText.Printf("%0.0f", pos);

        rotateTheLine(textCoord, blank);

        dc->SetTextForeground(*wxBLACK);
        dc->DrawRotatedText(posText, toScreenPoint(textCoord), -cfg->Get_Alpha());
    }
}

void ChartClass::Draw(wxDC *dc) {
    dc->SetBackground(wxBrush(wxColour(255, 255, 255)));
    dc->SetClippingRegion(wxRect(0, 0, screenWidth, screenHeight));
    dc->Clear();
    // Draw the red border
    dc->SetPen(wxPen(*wxRED));
    dc->DrawRectangle(0, 0, screenWidth, screenHeight);
    // Draw axis
    dc->SetPen(wxPen(*wxBLUE));
    drawAxis(dc);
    // Draw
    dc->SetPen(wxPen(*wxGREEN));
    double normalizator = (x_max - x_min) / x_step;
    for (int i = 1; i < x_step; i++) {
        double previousArg = (i - 1) * normalizator + x_min;
        double previousValue = GetFunctionValue(previousArg);
        wxRealPoint start(previousArg, previousValue);
        double currentArg = i * normalizator + x_min;
        double currentValue = GetFunctionValue(currentArg);
        wxRealPoint end(currentArg, currentValue);
        transformLine2D(getTranslation(cfg->Get_dX(), cfg->Get_dY()), start, end);
        rotateTheLine(start, end);
        dc->DrawLine(toScreenPoint(start), toScreenPoint(end));
    }

}
