//---------------------------------------------------------------------------
//
// Name:        Lab04App.cpp
// Author:      Janusz Malinowski
// Created:     2008-08-19 11:49:34
// Description: 
//
//---------------------------------------------------------------------------

#include "Lab04App.h"
#include "Lab04Dlg.h"

IMPLEMENT_APP(Lab04DlgApp)

bool Lab04DlgApp::OnInit() {
    Lab04Dlg *dialog = new Lab04Dlg(NULL);
    SetTopWindow(dialog);
    dialog->Show(true);
    return true;
}

int Lab04DlgApp::OnExit() {
    return 0;
}
