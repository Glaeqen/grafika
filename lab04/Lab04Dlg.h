//---------------------------------------------------------------------------
//
// Name:        Lab04Dlg.h
// Author:      Janusz Malinowski
// Created:     2008-08-19 11:49:34
// Description: Lab04Dlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __LAB04DLG_h__
#define __LAB04DLG_h__

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP

#include <wx/wx.h>
#include <wx/dialog.h>

#else
#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/filedlg.h>
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/radiobut.h>
#include <wx/scrolbar.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/panel.h>
#include <wx/sizer.h>
////Header Include End

class ConfigClass;

//#include "ConfigClass.h"

////Dialog Style Start
#undef Lab04Dlg_STYLE
#define Lab04Dlg_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class Lab04Dlg : public wxDialog {
private:
DECLARE_EVENT_TABLE()

public:
    Lab04Dlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Lab04"),
             const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize, long style = Lab04Dlg_STYLE);

    virtual ~Lab04Dlg();

    void WxButton1Click(wxCommandEvent &event);

    void Lab04DlgSize(wxSizeEvent &event);

    void WxPanelUpdateUI(wxUpdateUIEvent &event);

    void Lab04DlgSize1(wxSizeEvent &event);

    void Repaint();

    void onTimer(wxTimerEvent &event);

    void onTimerCheckboxOn(wxCommandEvent &event);

    void WxPanelUpdateUI1(wxUpdateUIEvent &event);

    void WxScrollBar_alphaScroll(wxScrollEvent &event);

    void WxButton1Click1(wxCommandEvent &event);

    void WxButton2Click(wxCommandEvent &event);

    void WxChoiceSelected(wxCommandEvent &event);

    void WxEdit_x0Updated(wxCommandEvent &event);

    void WxRB_MiddleClick(wxCommandEvent &event);

    void WxRB_CenterClick(wxCommandEvent &event);

    void WxEdit_dXUpdated(wxCommandEvent &event);

    void WxEdit_x1Updated(wxCommandEvent &event);

    void WxEdit_y0Updated(wxCommandEvent &event);

    void WxEdit_y1Updated(wxCommandEvent &event);

    void WxEdit_dYUpdated(wxCommandEvent &event);

    void WxEdit_x_startUpdated(wxCommandEvent &event);

    void WxEdit_x_stopUpdated(wxCommandEvent &event);

    void Lab04DlgInitDialog(wxInitDialogEvent &event);

    void WxButton4Click(wxCommandEvent &event);

    void WxButton3Click(wxCommandEvent &event);

private:
    //Do not add custom control declarations between
    //GUI Control Declaration Start and GUI Control Declaration End.
    //wxDev-C++ will remove them. Add custom code after the block.
    ////GUI Control Declaration Start
    wxCheckBox *isTimerOn;
    wxTimer *timer;

    wxFileDialog *WxSaveFileDialog1;
    wxFileDialog *WxOpenFileDialog1;
    wxButton *WxButton4;
    wxButton *WxButton3;
    wxBoxSizer *WxBoxSizer11;
    wxButton *WxButton2;
    wxBoxSizer *WxBoxSizer9;
    wxChoice *WxChoice;
    wxBoxSizer *WxBoxSizer10;
    wxStaticText *Ly_max;
    wxStaticText *WxStaticText15;
    wxStaticText *Ly_min;
    wxStaticText *WxStaticText14;
    wxBoxSizer *WxBoxSizer8;
    wxTextCtrl *WxEdit_x_stop;
    wxStaticText *WxStaticText13;
    wxTextCtrl *WxEdit_x_start;
    wxStaticText *WxStaticText12;
    wxBoxSizer *WxBoxSizer7;
    wxStaticText *WxStaticText11;
    wxTextCtrl *WxEdit_dY;
    wxStaticText *WxStaticText10;
    wxTextCtrl *WxEdit_dX;
    wxStaticText *WxStaticText9;
    wxBoxSizer *WxBoxSizer6;
    wxStaticText *WxStaticText8;
    wxRadioButton *WxRB_Center;
    wxRadioButton *WxRB_Middle;
    wxStaticText *WxStaticText_alpha;
    wxScrollBar *WxScrollBar_alpha;
    wxStaticText *WxStaticText7;
    wxBoxSizer *WxBoxSizer5;
    wxStaticText *WxStaticText6;
    wxTextCtrl *WxEdit_y1;
    wxStaticText *WxStaticText5;
    wxTextCtrl *WxEdit_x1;
    wxStaticText *WxStaticText4;
    wxBoxSizer *WxBoxSizer4;
    wxTextCtrl *WxEdit_y0;
    wxStaticText *WxStaticText3;
    wxTextCtrl *WxEdit_x0;
    wxStaticText *WxStaticText2;
    wxBoxSizer *WxBoxSizer3;
    wxStaticText *WxStaticText1;
    wxBoxSizer *WxBoxSizer2;
    wxPanel *WxPanel;
    wxBoxSizer *WxBoxSizer1;
    ////GUI Control Declaration End

private:
    //Note: if you receive any error with these enum IDs, then you need to
    //change your old form code that are based on the #define control IDs.
    //#defines may replace a numeric value for the enum names.
    //Try copy and pasting the below block in your old form header files.
    enum {
        ////GUI Enum Control ID Start
        TIMER_ID = 1070,
        ID_TIMER_CHECKBOX = 1059,
        ID_WXBUTTON4 = 1052,
        ID_WXBUTTON3 = 1051,
        ID_WXBUTTON2 = 1040,
        ID_WXCHOICE = 1047,
        ID_LY_MAX = 1037,
        ID_WXSTATICTEXT15 = 1036,
        ID_LY_MIN = 1035,
        ID_WXSTATICTEXT14 = 1034,
        ID_WXEDIT_X_STOP = 1032,
        ID_WXSTATICTEXT13 = 1031,
        ID_WXEDIT_X_START = 1030,
        ID_WXSTATICTEXT12 = 1029,
        ID_WXSTATICTEXT11 = 1027,
        ID_WXEDIT_DY = 1026,
        ID_WXSTATICTEXT10 = 1024,
        ID_WXEDIT_DX = 1023,
        ID_WXSTATICTEXT9 = 1022,
        ID_WXSTATICTEXT8 = 1020,
        ID_WXRB_CENTER = 1049,
        ID_WXRB_MIDDLE = 1048,
        ID_WXSTATICTEXT_ALPHA = 1019,
        ID_WXSCROLLBAR_ALPHA = 1018,
        ID_WXSTATICTEXT7 = 1017,
        ID_WXSTATICTEXT6 = 1015,
        ID_WXEDITY1 = 1014,
        ID_WXSTATICTEXT5 = 1013,
        ID_WXEDIT_X1 = 1012,
        ID_WXSTATICTEXT4 = 1011,
        ID_WXEDIT_Y0 = 1009,
        ID_WXSTATICTEXT3 = 1008,
        ID_WXEDIT_X0 = 1007,
        ID_WXSTATICTEXT2 = 1006,
        ID_WXSTATICTEXT1 = 1004,
        ID_WXPANEL = 1002,
        ////GUI Enum Control ID End
                ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
    };

private:
    void OnClose(wxCloseEvent &event);

    void CreateGUIControls();

private:
    ConfigClass *cfg;
public:
    void UpdateControls();
};

#endif
