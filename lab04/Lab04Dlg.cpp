//---------------------------------------------------------------------------
//
// Name:        Lab04Dlg.cpp
// Author:      Janusz Malinowski
// Created:     2008-08-19 11:49:34
// Description: Lab04Dlg class implementation
//
//---------------------------------------------------------------------------

#include "Lab04Dlg.h"
#include <wx/dcbuffer.h>
#include "ConfigClass.h"
#include "ChartClass.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

int funkcja = 0;

//----------------------------------------------------------------------------
// Lab04Dlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(Lab04Dlg, wxDialog)
                ////Manual Code Start
                ////Manual Code End
                EVT_TIMER(TIMER_ID, Lab04Dlg::onTimer)
				EVT_CHECKBOX(ID_TIMER_CHECKBOX, Lab04Dlg::onTimerCheckboxOn)
                EVT_CLOSE(Lab04Dlg::OnClose)
                EVT_SIZE(Lab04Dlg::Lab04DlgSize1)
                EVT_INIT_DIALOG(Lab04Dlg::Lab04DlgInitDialog)
                EVT_BUTTON(ID_WXBUTTON4, Lab04Dlg::WxButton4Click)
                EVT_BUTTON(ID_WXBUTTON3, Lab04Dlg::WxButton3Click)
                EVT_BUTTON(ID_WXBUTTON2, Lab04Dlg::WxButton2Click)
                EVT_CHOICE(ID_WXCHOICE, Lab04Dlg::WxChoiceSelected)

                EVT_TEXT(ID_WXEDIT_X_STOP, Lab04Dlg::WxEdit_x_stopUpdated)

                EVT_TEXT(ID_WXEDIT_X_START, Lab04Dlg::WxEdit_x_startUpdated)

                EVT_TEXT(ID_WXEDIT_DY, Lab04Dlg::WxEdit_dYUpdated)

                EVT_TEXT(ID_WXEDIT_DX, Lab04Dlg::WxEdit_dXUpdated)
                EVT_RADIOBUTTON(ID_WXRB_CENTER, Lab04Dlg::WxRB_CenterClick)
                EVT_RADIOBUTTON(ID_WXRB_MIDDLE, Lab04Dlg::WxRB_MiddleClick)

                EVT_COMMAND_SCROLL(ID_WXSCROLLBAR_ALPHA, Lab04Dlg::WxScrollBar_alphaScroll)

                EVT_TEXT(ID_WXEDITY1, Lab04Dlg::WxEdit_y1Updated)

                EVT_TEXT(ID_WXEDIT_X1, Lab04Dlg::WxEdit_x1Updated)

                EVT_TEXT(ID_WXEDIT_Y0, Lab04Dlg::WxEdit_y0Updated)

                EVT_TEXT(ID_WXEDIT_X0, Lab04Dlg::WxEdit_x0Updated)

                EVT_UPDATE_UI(ID_WXPANEL, Lab04Dlg::WxPanelUpdateUI1)
END_EVENT_TABLE()
////Event Table End

Lab04Dlg::Lab04Dlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize &size,
    long style)
    : wxDialog(parent, id, title, position, size, style) {
        cfg = new ConfigClass(this);
        CreateGUIControls();
}

Lab04Dlg::~Lab04Dlg() {
    delete cfg;
}

void Lab04Dlg::CreateGUIControls() {
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End.
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start
    wxString tempString;

    WxBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    this->SetSizer(WxBoxSizer1);
    this->SetAutoLayout(true);

    WxPanel = new wxPanel(this, ID_WXPANEL, wxPoint(5, 246), wxSize(600, 41));
    WxPanel->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer1->Add(WxPanel, 1, wxALIGN_LEFT | wxEXPAND | wxALL, 5);

    WxBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    WxBoxSizer1->Add(WxBoxSizer2, 0, wxALIGN_RIGHT | wxALL, 5);

    isTimerOn = new wxCheckBox(this, ID_TIMER_CHECKBOX, wxT("Timer On"), wxPoint(5, 5));
    isTimerOn->SetValue(false);
    WxBoxSizer2->Add(isTimerOn, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText1 = new wxStaticText(this, ID_WXSTATICTEXT1, wxT("Układ świata"), wxPoint(80, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText1"));
    WxStaticText1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxStaticText1, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer3, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText2 = new wxStaticText(this, ID_WXSTATICTEXT2, wxT("x0"), wxPoint(5, 6), wxDefaultSize, 0,
                                     wxT("WxStaticText2"));
    WxStaticText2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxStaticText2, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_x0());
    WxEdit_x0 = new wxTextCtrl(this, ID_WXEDIT_X0, tempString, wxPoint(31, 5), wxSize(45, 19), 0, wxDefaultValidator,
                               wxT("WxEdit_x0"));
    WxEdit_x0->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxEdit_x0, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText3 = new wxStaticText(this, ID_WXSTATICTEXT3, wxT("y0"), wxPoint(86, 6), wxDefaultSize, 0,
                                     wxT("WxStaticText3"));
    WxStaticText3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxStaticText3, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_y0());
    WxEdit_y0 = new wxTextCtrl(this, ID_WXEDIT_Y0, tempString, wxPoint(112, 5), wxSize(45, 19), 0, wxDefaultValidator,
                               wxT("WxEdit_y0"));
    WxEdit_y0->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxEdit_y0, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer4, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText4 = new wxStaticText(this, ID_WXSTATICTEXT4, wxT("x1"), wxPoint(5, 6), wxDefaultSize, 0,
                                     wxT("WxStaticText4"));
    WxStaticText4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxStaticText4, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_x1());
    WxEdit_x1 = new wxTextCtrl(this, ID_WXEDIT_X1, tempString, wxPoint(31, 5), wxSize(45, 19), 0, wxDefaultValidator,
                               wxT("WxEdit_x1"));
    WxEdit_x1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxEdit_x1, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText5 = new wxStaticText(this, ID_WXSTATICTEXT5, wxT("y1"), wxPoint(86, 6), wxDefaultSize, 0,
                                     wxT("WxStaticText5"));
    WxStaticText5->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxStaticText5, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_y1());
    WxEdit_y1 = new wxTextCtrl(this, ID_WXEDITY1, tempString, wxPoint(112, 5), wxSize(45, 19), 0, wxDefaultValidator,
                               wxT("WxEdit_y1"));
    WxEdit_y1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxEdit_y1, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText6 = new wxStaticText(this, ID_WXSTATICTEXT6, wxT("Obrót"), wxPoint(96, 110), wxDefaultSize, 0,
                                     wxT("WxStaticText6"));
    WxStaticText6->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxStaticText6, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer5, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText7 = new wxStaticText(this, ID_WXSTATICTEXT7, wxT("alpha"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText7"));
    WxStaticText7->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer5->Add(WxStaticText7, 0, wxALIGN_CENTER | wxALL, 5);

    WxScrollBar_alpha = new wxScrollBar(this, ID_WXSCROLLBAR_ALPHA, wxPoint(45, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                        wxDefaultValidator, wxT("WxScrollBar_alpha"));
    WxScrollBar_alpha->Enable(false);
    WxScrollBar_alpha->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer5->Add(WxScrollBar_alpha, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_Alpha());
    WxStaticText_alpha = new wxStaticText(this, ID_WXSTATICTEXT_ALPHA, tempString, wxPoint(176, 5), wxDefaultSize, 0,
                                          wxT("WxStaticText_alpha"));
    WxStaticText_alpha->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer5->Add(WxStaticText_alpha, 0, wxALIGN_CENTER | wxALL, 5);

    WxRB_Middle = new wxRadioButton(this, ID_WXRB_MIDDLE, wxT("Środek ekranu"), wxPoint(55, 174), wxSize(113, 17), 0,
                                    wxDefaultValidator, wxT("WxRB_Middle"));
    WxRB_Middle->SetValue(true);
    WxRB_Middle->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxRB_Middle, 0, wxALIGN_CENTER | wxALL, 5);

    WxRB_Center = new wxRadioButton(this, ID_WXRB_CENTER, wxT("Środek układu"), wxPoint(55, 201), wxSize(113, 17), 0,
                                    wxDefaultValidator, wxT("WxRB_Center"));
    WxRB_Center->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxRB_Center, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText8 = new wxStaticText(this, ID_WXSTATICTEXT8, wxT("Translacja o wektor"), wxPoint(63, 228),
                                     wxDefaultSize, 0, wxT("WxStaticText8"));
    WxStaticText8->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxStaticText8, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer6, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText9 = new wxStaticText(this, ID_WXSTATICTEXT9, wxT("dX"), wxPoint(5, 6), wxDefaultSize, 0,
                                     wxT("WxStaticText9"));
    WxStaticText9->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxStaticText9, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_dX());
    WxEdit_dX = new wxTextCtrl(this, ID_WXEDIT_DX, tempString, wxPoint(31, 5), wxSize(45, 19), 0, wxDefaultValidator,
                               wxT("WxEdit_dX"));
    WxEdit_dX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxEdit_dX, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText10 = new wxStaticText(this, ID_WXSTATICTEXT10, wxT("dY"), wxPoint(86, 6), wxDefaultSize, 0,
                                      wxT("WxStaticText10"));
    WxStaticText10->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxStaticText10, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_dY());
    WxEdit_dY = new wxTextCtrl(this, ID_WXEDIT_DY, tempString, wxPoint(112, 5), wxSize(45, 19), 0, wxDefaultValidator,
                               wxT("WxEdit_dY"));
    WxEdit_dY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxEdit_dY, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText11 = new wxStaticText(this, ID_WXSTATICTEXT11, wxT("Wartości na wykresie:"), wxPoint(57, 294),
                                      wxDefaultSize, 0, wxT("WxStaticText11"));
    WxStaticText11->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxStaticText11, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer7, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText12 = new wxStaticText(this, ID_WXSTATICTEXT12, wxT("x_start:"), wxPoint(5, 6), wxDefaultSize, 0,
                                      wxT("WxStaticText12"));
    WxStaticText12->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxStaticText12, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_x_start());
    WxEdit_x_start = new wxTextCtrl(this, ID_WXEDIT_X_START, tempString, wxPoint(58, 5), wxSize(45, 19), 0,
                                    wxDefaultValidator, wxT("WxEdit_x_start"));
    WxEdit_x_start->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxEdit_x_start, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText13 = new wxStaticText(this, ID_WXSTATICTEXT13, wxT("x_stop:"), wxPoint(113, 6), wxDefaultSize, 0,
                                      wxT("WxStaticText13"));
    WxStaticText13->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxStaticText13, 0, wxALIGN_CENTER | wxALL, 5);

    tempString.Printf(wxT("%2.1lf"), cfg->Get_x_stop());
    WxEdit_x_stop = new wxTextCtrl(this, ID_WXEDIT_X_STOP, tempString, wxPoint(164, 5), wxSize(45, 19), 0,
                                   wxDefaultValidator, wxT("WxEdit_x_stop"));
    WxEdit_x_stop->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxEdit_x_stop, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer8 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer8, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText14 = new wxStaticText(this, ID_WXSTATICTEXT14, wxT("y_min:"), wxPoint(5, 5), wxDefaultSize, 0,
                                      wxT("WxStaticText14"));
    WxStaticText14->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(WxStaticText14, 0, wxALIGN_CENTER | wxALL, 5);

    Ly_min = new wxStaticText(this, ID_LY_MIN, wxT("?"), wxPoint(51, 5), wxDefaultSize, 0, wxT("Ly_min"));
    Ly_min->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(Ly_min, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText15 = new wxStaticText(this, ID_WXSTATICTEXT15, wxT("y_max:"), wxPoint(70, 5), wxDefaultSize, 0,
                                      wxT("WxStaticText15"));
    WxStaticText15->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(WxStaticText15, 0, wxALIGN_CENTER | wxALL, 5);

    Ly_max = new wxStaticText(this, ID_LY_MAX, wxT("?"), wxPoint(120, 5), wxDefaultSize, 0, wxT("Ly_max"));
    Ly_max->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(Ly_max, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer10 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer10, 0, wxALIGN_CENTER | wxALL, 5);

    wxArrayString arrayStringFor_WxChoice;
    arrayStringFor_WxChoice.Add(wxT("x+sin(4x)"));
    arrayStringFor_WxChoice.Add(wxT("x^2"));
    arrayStringFor_WxChoice.Add(wxT("0.5*e^(4x-3x^2)"));
    WxChoice = new wxChoice(this, ID_WXCHOICE, wxPoint(5, 5), wxSize(145, 30), arrayStringFor_WxChoice, 0,
                            wxDefaultValidator, wxT("WxChoice"));
    WxChoice->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxChoice->SetSelection(cfg->Get_F_type());
    WxBoxSizer10->Add(WxChoice, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer9 = new wxBoxSizer(wxVERTICAL);
    WxBoxSizer2->Add(WxBoxSizer9, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton2 = new wxButton(this, ID_WXBUTTON2, wxT("Do układu świata"), wxPoint(38, 5), wxSize(104, 25), 0,
                             wxDefaultValidator, wxT("WxButton2"));
    WxButton2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer9->Add(WxButton2, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer11 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer9->Add(WxBoxSizer11, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton3 = new wxButton(this, ID_WXBUTTON3, wxT("Wczytaj"), wxPoint(5, 5), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton3"));
    WxButton3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer11->Add(WxButton3, 0, wxALIGN_CENTER | wxALL, 5);

    WxButton4 = new wxButton(this, ID_WXBUTTON4, wxT("Zapisz"), wxPoint(90, 5), wxSize(75, 25), 0, wxDefaultValidator,
                             wxT("WxButton4"));
    WxButton4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer11->Add(WxButton4, 0, wxALIGN_CENTER | wxALL, 5);

    WxOpenFileDialog1 = new wxFileDialog(this, wxT("Choose a file"), wxT(""), wxT(""), wxT("*.*"), wxFD_OPEN);

    WxSaveFileDialog1 = new wxFileDialog(this, wxT("Choose a file"), wxT(""), wxT(""), wxT("*.*"), wxFD_SAVE);

    SetTitle(wxT("Lab04"));
    SetIcon(wxNullIcon);

    GetSizer()->Layout();
    GetSizer()->Fit(this);
    GetSizer()->SetSizeHints(this);
    Center();

    ////GUI Items Creation End
    WxScrollBar_alpha->SetScrollbar(static_cast<int>(cfg->Get_Alpha()), 1, 360, 1, true);
    WxScrollBar_alpha->SetScrollbar(static_cast<int>(cfg->Get_Alpha()), 1, 360, 1, true);
    WxScrollBar_alpha->Enable(true);

    timer = new wxTimer(this, TIMER_ID);
}

void Lab04Dlg::Repaint() {
    wxClientDC dc1(WxPanel);
    wxBufferedDC dc(&dc1);

    int w, h;
    WxPanel->GetSize(&w, &h);
    ChartClass MyChart(cfg, w, h);
    MyChart.Draw(&dc);

}

void Lab04Dlg::onTimer(wxTimerEvent &event) {
    cfg->Set_Alpha(static_cast<int>(cfg->Get_Alpha()+6.0)%360);
    WxScrollBar_alpha->SetScrollbar(static_cast<int>(cfg->Get_Alpha()), 1, 360, 1, true);
    wxString t;
    t << WxScrollBar_alpha->GetThumbPosition();
    WxStaticText_alpha->SetLabel(t);
    Repaint();
}

void Lab04Dlg::onTimerCheckboxOn(wxCommandEvent &event){
	if(event.IsChecked()){
        if(!timer->IsRunning()){
            timer->Start(1000);
        }
    }
    else{
        timer->Stop();
    }
}

void Lab04Dlg::OnClose(wxCloseEvent & /*event*/) {
    Destroy();
}

/*
 * WxButton1Click
 */
void Lab04Dlg::WxButton1Click(wxCommandEvent &event) {
    // insert your code here
    Repaint();
}

/*
 * Lab04DlgSize1
 */
void Lab04Dlg::Lab04DlgSize1(wxSizeEvent &event) {
    // insert your code here
    Layout();
    int width, height;
    wxString t;
    WxPanel->GetSize(&width, &height);
    t << width << "x" << height;
//	WxStaticText1->SetLabel(t);
}

/*
 * WxPanelUpdateUI1
 */
void Lab04Dlg::WxPanelUpdateUI1(wxUpdateUIEvent &event) {
    // insert your code here
    Repaint();
}

/*
 * WxScrollBar_alphaScroll
 */
void Lab04Dlg::WxScrollBar_alphaScroll(wxScrollEvent &event) {
    // insert your code here
    wxString t;
    t << WxScrollBar_alpha->GetThumbPosition();
    WxStaticText_alpha->SetLabel(t);
    cfg->Set_Alpha(WxScrollBar_alpha->GetThumbPosition());
    Repaint();
}


/*
 * WxButton2Click
 */
void Lab04Dlg::WxButton2Click(wxCommandEvent &event) {
    // insert your code here

/*	double MyChart.x_min,MyChart.x_max,MyChart.y_min,ymax;
	wxString t,str;

	str=WxEdit_x_start->GetValue();str.ToDouble(&x_start);
	str=WxEdit_x_stop->GetValue();str.ToDouble(&x_stop);

    wykres(&MyChart.x_min,&MyChart.x_max,&MyChart.y_min,&ymax);
    t.Printf(wxT("%2.3lf"),MyChart.y_min);Ly_min->SetLabel(t);
    WxEdit_y0->SetValue(t);
	t.Printf(wxT("%2.3lf"),ymax);Ly_max->SetLabel(t);
	WxEdit_y1->SetValue(t);
*/
    WxEdit_x0->SetValue(WxEdit_x_start->GetValue());
    WxEdit_x1->SetValue(WxEdit_x_stop->GetValue());

    //Layout();
    Repaint();
}

/*
 * WxChoiceSelected
 */
void Lab04Dlg::WxChoiceSelected(wxCommandEvent &event) {
    // insert your code here
    cfg->Set_F_type(WxChoice->GetSelection());
    wxString str;
    ChartClass c(cfg);
    str.Printf(wxT("%2.1lf"), c.Get_Y_min());
    Ly_min->SetLabel(str);

    str.Printf(wxT("%2.1lf"), c.Get_Y_max());
    Ly_max->SetLabel(str);
    Repaint();
}

/*
 str=WxEdit_x0->GetValue();str.ToDouble(&x0);
 str=WxEdit_x1->GetValue();str.ToDouble(&x1);
 str=WxEdit_y0->GetValue();str.ToDouble(&y0);
 str=WxEdit_y1->GetValue();str.ToDouble(&y1);
 WxScrollBar_alpha->GetThumbPosition()
  str=WxEdit_dX->GetValue();str.ToDouble(&x0);
 str=WxEdit_dY->GetValue();str.ToDouble(&y0);

*/

/*
 * WxEdit_x0Updated
 */
void Lab04Dlg::WxEdit_x0Updated(wxCommandEvent &event) {
    double v;
    if (WxEdit_x0) {
        wxString str = WxEdit_x0->GetValue();
        str.ToDouble(&v);
        cfg->Set_x0(v);
        Repaint();
    }
}

/*
 * WxRB_MiddleClick
 */
void Lab04Dlg::WxRB_MiddleClick(wxCommandEvent &event) {
    // insert your code here
    cfg->SetRotateScreen(true);
    Repaint();
}

/*
 * WxRB_CenterClick
 */
void Lab04Dlg::WxRB_CenterClick(wxCommandEvent &event) {
    // insert your code here
    cfg->SetRotateScreen(false);
    Repaint();
}

/*
 * WxEdit_dXUpdated
 */
void Lab04Dlg::WxEdit_dXUpdated(wxCommandEvent &event) {
    // insert your code here
    double v;
    if (WxEdit_dX) {
        wxString str = WxEdit_dX->GetValue();
        str.ToDouble(&v);
        cfg->Set_dX(v);
        Repaint();
    }
}

/*
 * WxEdit_x1Updated
 */
void Lab04Dlg::WxEdit_x1Updated(wxCommandEvent &event) {
    // insert your code here
    double v;
    if (WxEdit_x1) {
        wxString str = WxEdit_x1->GetValue();
        str.ToDouble(&v);
        cfg->Set_x1(v);
        Repaint();
    }

}

/*
 * WxEdit_y0Updated
 */
void Lab04Dlg::WxEdit_y0Updated(wxCommandEvent &event) {
    // insert your code here
    double v;
    if (WxEdit_y0) {
        wxString str = WxEdit_y0->GetValue();
        str.ToDouble(&v);
        cfg->Set_y0(v);
        Repaint();
    }
}

/*
 * WxEdit_y1Updated
 */
void Lab04Dlg::WxEdit_y1Updated(wxCommandEvent &event) {
    // insert your code here
    double v;
    if (WxEdit_y1) {
        wxString str = WxEdit_y1->GetValue();
        str.ToDouble(&v);
        cfg->Set_y1(v);
        Repaint();
    }

}

/*
 * WxEdit_dYUpdated
 */
void Lab04Dlg::WxEdit_dYUpdated(wxCommandEvent &event) {
    // insert your code here
    double v;
    if (WxEdit_dY) {
        wxString str = WxEdit_dY->GetValue();
        str.ToDouble(&v);
        cfg->Set_dY(v);
        Repaint();
    }
}

/*
 * WxEdit_x_startUpdated
 */
void Lab04Dlg::WxEdit_x_startUpdated(wxCommandEvent &event) {
    // insert your code here
    double v;
    wxString str;
    if (WxEdit_x_start) {
        str = WxEdit_x_start->GetValue();
        str.ToDouble(&v);
        cfg->Set_x_start(v);
        ChartClass MChart(cfg);
        str.Printf(wxT("%2.4lf"), MChart.Get_Y_min());
        Ly_min->SetLabel(str);
        Layout();
        Repaint();
    }
}

/*
 * WxEdit_x_stopUpdated
 */
void Lab04Dlg::WxEdit_x_stopUpdated(wxCommandEvent &event) {
    // insert your code here
    double v;
    wxString str;
    if (WxEdit_x_stop) {
        str = WxEdit_x_stop->GetValue();
        str.ToDouble(&v);
        cfg->Set_x_stop(v);
        ChartClass MChart(cfg);
        str.Printf(wxT("%2.4lf"), MChart.Get_Y_max());
        Ly_max->SetLabel(str);
        Layout();
        Repaint();
    }
}

/*
 * Lab04DlgInitDialog
 */
void Lab04Dlg::Lab04DlgInitDialog(wxInitDialogEvent &event) {
    // insert your code here
    wxString str;
    double v;
    if (WxEdit_x_start) {
        str = WxEdit_x_start->GetValue();
        str.ToDouble(&v);
        cfg->Set_x_start(v);
        ChartClass MChart(cfg);
        str.Printf(wxT("%2.4lf"), MChart.Get_Y_min());
        Ly_min->SetLabel(str);
    }
    if (WxEdit_x_stop) {
        str = WxEdit_x_stop->GetValue();
        str.ToDouble(&v);
        cfg->Set_x_stop(v);
        ChartClass MChart(cfg);
        str.Printf(wxT("%2.4lf"), MChart.Get_Y_max());
        Ly_max->SetLabel(str);
    }
    Layout();
}

/*
 * WxButton4Click
 */
void Lab04Dlg::WxButton4Click(wxCommandEvent &event) {
    // insert your code here
    wxString filename;
    WxSaveFileDialog1->SetWildcard("config files (*.cfg)|*.cfg");
    if (WxSaveFileDialog1->ShowModal() == wxID_OK) {
        filename = WxSaveFileDialog1->GetPath();
        cfg->Save((char *) filename.c_str().AsChar());
    }
}

/*
 * WxButton3Click
 */
void Lab04Dlg::WxButton3Click(wxCommandEvent &event) {
    // insert your code here
    wxString filename;
    WxOpenFileDialog1->SetWildcard("config files (*.cfg)|*.cfg");
    if (WxOpenFileDialog1->ShowModal() == wxID_OK) {
        filename = WxOpenFileDialog1->GetPath();
        cfg->Load((char *) filename.c_str().AsChar());
    }
}

void Lab04Dlg::UpdateControls() {
    wxString str;
    str.Printf(wxT("%2.1lf"), cfg->Get_x0());
    WxEdit_x0->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_x1());
    WxEdit_x1->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_y0());
    WxEdit_y0->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_y1());
    WxEdit_y1->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_dX());
    WxEdit_dX->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_dY());
    WxEdit_dY->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_x_start());
    WxEdit_x_start->SetLabel(str);
    str.Printf(wxT("%2.1lf"), cfg->Get_x_stop());
    WxEdit_x_stop->SetLabel(str);
    WxScrollBar_alpha->SetThumbPosition(int(cfg->Get_Alpha()));
    str.Printf(wxT("%4.1lf"), (double) WxScrollBar_alpha->GetThumbPosition());
    WxStaticText_alpha->SetLabel(str);
}
