#include <string>
#include <cmath>

#include "SFML/Graphics.hpp"
#include "HSL.h"
#include "HSV.h"
#include "CMY.h"
#include "RGB.h"
#include "Slider.h"
#include "EventInfo.h"



int main(){
	sf::RenderWindow window(sf::VideoMode(800, 600), "", sf::Style::None);
	sf::Event event;

	EventInfo eventInfo;

	sf::Clock clock;
	sf::Time time = sf::Time::Zero;
	
	unsigned int FPS = 0 , frame_counter = 0;

	sf::Font font;
	font.loadFromFile("verdana.ttf");

	sf::Text fpsText;
	fpsText.setPosition(window.getSize().x-75, 3);
	fpsText.setFont(font);
	fpsText.setCharacterSize(13);
	fpsText.setStyle(sf::Text::Regular);
	fpsText.setFillColor(sf::Color::White);

	unsigned radius = 100;

	HSL hsl(radius);
	HSV hsv(radius);
	CMY cmy(radius);
	RGB rgb(radius);
	Slider slider;

	hsl.setPosition(20, 40);
	hsv.setPosition(320, 40);
	cmy.setPosition(20, 340);
	rgb.setPosition(320, 340);
	slider.setPosition(window.getSize().x-130, 140);

	clock.restart().asMilliseconds();
	while (window.isOpen()){
		window.clear(sf::Color::Black);

		while (window.pollEvent(event)){
			switch(event.type){
				case sf::Event::Closed:
					window.close();
					break;
				case sf::Event::MouseButtonPressed:
					eventInfo.isReleased = false;
					eventInfo.isClicked = true;
					eventInfo.mousePos.x = event.mouseButton.x;
					eventInfo.mousePos.y = event.mouseButton.y;
					break;
				case sf::Event::MouseButtonReleased:
					eventInfo.isReleased = true;
					eventInfo.isClicked = false;
				case sf::Event::MouseMoved:
					eventInfo.mousePos.x = event.mouseMove.x;
					eventInfo.mousePos.y = event.mouseMove.y;
			}
		}
		if (clock.getElapsedTime().asSeconds() >= 1.0f){
			FPS = (unsigned int)((float)frame_counter / clock.getElapsedTime().asSeconds());
			clock.restart();
			
			frame_counter = 0;
		}
		frame_counter++;

		slider.updateMarkerAndParameter(eventInfo);
		hsl.setLvalue(slider.getParameter());
		hsv.setVvalue(slider.getParameter());
		cmy.setYvalue(slider.getParameter());
		rgb.setBvalue(slider.getParameter());

		fpsText.setString("FPS: " + std::to_string(FPS));
		window.draw(fpsText);
		window.draw(hsl);
		window.draw(hsv);
		window.draw(cmy);
		window.draw(rgb);
		window.draw(slider);

		window.display();
	}
}