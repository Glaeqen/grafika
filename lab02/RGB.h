#pragma once
#include "ColorRepresentation.h"

class RGB : public ColorRepresentation{
	float b_;

	virtual sf::Color convertMeToRGB(float angle, float distanceFromCenter) const;
public:
	RGB(unsigned radius);
	void setBvalue(float b);
	float getBvalue() const;
};