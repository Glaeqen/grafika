#include <cmath>
#include "HSV.h"

sf::Color HSV::convertMeToRGB(float angle, float distanceFromCenter) const{
	float h = angle; // Radians
	float s = distanceFromCenter / radius_;
	float c = v_ * s;

	float x = c * (1 - fabs(fmod(h*3/PI, 2) - 1));
	float m = v_ - c;

	float r, g, b;
	if(h <= PI/3) {r = c; g = x; b = 0;}
	else if(h <= 2*PI/3) {r = x; g = c; b = 0;}
	else if(h <= PI) {r = 0; g = c; b = x;}
	else if(h <= 4*PI/3) {r = 0; g = x; b = c;}
	else if(h <= 5*PI/3) {r = x; g = 0; b = c;}
	else if(h <= 2*PI) {r = c; g = 0; b = x;}
	else r = g = b = -255; // Should never happen

	return sf::Color((r+m)*255, (g+m)*255, (b+m)*255);
}

HSV::HSV(unsigned radius) : ColorRepresentation(radius){
	v_ = 0.5; // Default value value

	initialise();
	title_.setString("HSV");
	currentValue_.setString(std::string("V=") + std::to_string(v_));
}

void HSV::setVvalue(float v){
	if(fabs(v - v_) < brink_)	return;
	v_ = v;

	// Update texture after value change.
	fillColorArray();
	texture_->update(colorArray_);
	currentValue_.setString(std::string("V=") + std::to_string(v_));
}

float HSV::getVvalue() const{
	return v_;
}
