#pragma once
#include "ColorRepresentation.h"

class HSV : public ColorRepresentation{
	float v_;

	virtual sf::Color convertMeToRGB(float angle, float distanceFromCenter) const;
public:
	HSV(unsigned radius);
	void setVvalue(float v);
	float getVvalue() const;
};