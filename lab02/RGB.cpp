#include <cmath>
#include "RGB.h"

sf::Color RGB::convertMeToRGB(float angle, float distanceFromCenter) const{
	return sf::Color((distanceFromCenter/radius_)*255, (angle/(2*PI))*255, b_*255);
}

RGB::RGB(unsigned radius) : ColorRepresentation(radius){
	b_ = 0.5; // Default blue color value

	initialise();
	title_.setString("RGB");
	currentValue_.setString(std::string("B=") + std::to_string(static_cast<int>(b_*255)));
}

void RGB::setBvalue(float b){
	if(fabs(b - b_) < brink_) return;
	b_ = b;

	// Update texture after blue color change.
	fillColorArray();
	texture_->update(colorArray_);
	currentValue_.setString(std::string("B=") + std::to_string(static_cast<int>(b_*255)));
}

float RGB::getBvalue() const{
	return b_;
}
