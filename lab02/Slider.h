#pragma once
#include "EventInfo.h"
#include "SFML/Graphics.hpp"

class Slider : public sf::Drawable, public sf::Transformable{
	unsigned xSize_;
	unsigned ySize_;

	float parameter;

	sf::VertexArray marker_;

	sf::Uint8 *sliderColorArray_;
	sf::Texture *texture_;
	sf::Sprite *sprite_;

	void createTexture();
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
public:
	Slider(unsigned xSize = 40, unsigned ySize = 300);

	void updateMarkerAndParameter(EventInfo& eventInfo);

	float getParameter() const;

	~Slider();
};