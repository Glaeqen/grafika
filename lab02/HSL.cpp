#include <cmath>
#include <string>
#include "HSL.h"

sf::Color HSL::convertMeToRGB(float angle, float distanceFromCenter) const{
	float h = angle; // Radians
	float s = distanceFromCenter / radius_;
	float c = (1 - fabs(2*l_ - 1)) * s;

	float x = c * (1 - fabs(fmod(h*3/PI, 2) - 1));
	float m = l_ - c/2;

	float r, g, b;
	if(h <= PI/3) {r = c; g = x; b = 0;}
	else if(h <= 2*PI/3) {r = x; g = c; b = 0;}
	else if(h <= PI) {r = 0; g = c; b = x;}
	else if(h <= 4*PI/3) {r = 0; g = x; b = c;}
	else if(h <= 5*PI/3) {r = x; g = 0; b = c;}
	else if(h <= 2*PI) {r = c; g = 0; b = x;}
	else r = g = b = -255; // Should never happen

	return sf::Color((r+m)*255, (g+m)*255, (b+m)*255);
}

HSL::HSL(unsigned radius) : ColorRepresentation(radius){
	l_ = 0.5; // Default lightness value

	initialise();
	title_.setString("HSL");
	currentValue_.setString(std::string("L=") + std::to_string(l_));
}

void HSL::setLvalue(float l){
	if(fabs(l - l_) < brink_)	return;
	l_ = l;

	// Update texture after lightness change.
	fillColorArray();
	texture_->update(colorArray_);
	currentValue_.setString(std::string("L=") + std::to_string(l_));
}

float HSL::getLvalue() const{
	return l_;
}