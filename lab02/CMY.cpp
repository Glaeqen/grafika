#include <cmath>
#include "CMY.h"

sf::Color CMY::convertMeToRGB(float angle, float distanceFromCenter) const{
	return sf::Color(255-(distanceFromCenter/radius_)*255, 255-(angle/(2*PI))*255, 255-y_*255);
}

CMY::CMY(unsigned radius) : ColorRepresentation(radius){
	y_ = 0.5; // Default yellow color value

	initialise();
	title_.setString("CMY");
	currentValue_.setString(std::string("Y=") + std::to_string(y_*100) + '%');
}

void CMY::setYvalue(float y){
	if(fabs(y - y_) < brink_)	return;
	y_ = y;

	// Update texture after yellow color change.
	fillColorArray();
	texture_->update(colorArray_);
	currentValue_.setString(std::string("Y=") + std::to_string(y_*100) + '%');
}

float CMY::getYvalue() const{
	return y_;
}