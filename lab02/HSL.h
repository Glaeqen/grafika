#pragma once
#include "ColorRepresentation.h"

class HSL : public ColorRepresentation{
	float l_;

	virtual sf::Color convertMeToRGB(float angle, float distanceFromCenter) const;
public:
	HSL(unsigned radius);
	void setLvalue(float l);
	float getLvalue() const;
};