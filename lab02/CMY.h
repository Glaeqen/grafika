#pragma once
#include "ColorRepresentation.h"

class CMY : public ColorRepresentation{
	float y_;

	virtual sf::Color convertMeToRGB(float angle, float distanceFromCenter) const;
public:
	CMY(unsigned radius);
	void setYvalue(float y);
	float getYvalue() const;
};