#include "Slider.h"

void Slider::createTexture(){
	for(int x=0; x<xSize_; x++){
		for(int y=0; y<ySize_; y++){
			if(x == 0 || y == 0 || x == xSize_-1 || y == ySize_-1){
				sliderColorArray_[4 * (y * xSize_ + x) + 0] = 255;
				sliderColorArray_[4 * (y * xSize_ + x) + 1] = 255;
				sliderColorArray_[4 * (y * xSize_ + x) + 2] = 255;
				sliderColorArray_[4 * (y * xSize_ + x) + 3] = 255;				
			}
			else{
				sliderColorArray_[4 * (y * xSize_ + x) + 0] = y*255/ySize_;
				sliderColorArray_[4 * (y * xSize_ + x) + 1] = y*255/ySize_;
				sliderColorArray_[4 * (y * xSize_ + x) + 2] = y*255/ySize_;
				sliderColorArray_[4 * (y * xSize_ + x) + 3] = 255;
			}
		}
	}
}

void Slider::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	states.transform *= getTransform();
	target.draw(marker_, states);
	target.draw(*sprite_, states);
}

Slider::Slider(unsigned xSize, unsigned ySize):
	xSize_(xSize),
	ySize_(ySize),
	parameter(0.5),
	marker_(sf::Lines, 2),
	sliderColorArray_(new sf::Uint8[4*xSize_*ySize_]),
	texture_(new sf::Texture()),
	sprite_(new sf::Sprite())
{
	createTexture();
	texture_->create(xSize_, ySize_);
	texture_->update(sliderColorArray_);

	marker_[0] = sf::Vertex(sf::Vector2f(-5, ySize_/2), sf::Color::Red);
	marker_[1] = sf::Vertex(sf::Vector2f(xSize_+5, ySize_/2), sf::Color::Red);

	sprite_->setTexture(*texture_);
}

void Slider::updateMarkerAndParameter(EventInfo& eventInfo){
	static bool wasClickedInsideSlider = false;
	sf::Vector2f position = getPosition();
	if(eventInfo.isClicked){
		if(eventInfo.mousePos.x >= position.x)
		if(eventInfo.mousePos.x <= position.x + xSize_)
		if(eventInfo.mousePos.y >= position.y)
		if(eventInfo.mousePos.y <= position.y + ySize_){
			wasClickedInsideSlider = true;
		}
	}
	else wasClickedInsideSlider = false;
	if(wasClickedInsideSlider){
		if(eventInfo.mousePos.y > position.y)
		if(eventInfo.mousePos.y <= position.y + ySize_){
			marker_[0].position.y = marker_[1].position.y = eventInfo.mousePos.y - position.y;
			parameter = (eventInfo.mousePos.y - position.y)/(ySize_*1.0);
		}
	}
}

float Slider::getParameter() const{
	return parameter;
}

Slider::~Slider(){
	delete[] sliderColorArray_;
	delete texture_;
	delete sprite_;
}