#include "ColorRepresentation.h"
#include <cmath>
#include <iostream>

ColorRepresentation::ColorRepresentation(unsigned radius):
	PI(4*atan(1)),
	brink_(0.01),
	radius_(radius),
	colorArray_(new sf::Uint8[2*radius_*2*radius_*4]),
	texture_(new sf::Texture()),
	sprite_(new sf::Sprite())
{}

void ColorRepresentation::initialise(){
	font_.loadFromFile("verdana.ttf");

	title_.setFont(font_);
	title_.setCharacterSize(13);
	title_.setStyle(sf::Text::Regular);
	title_.setFillColor(sf::Color::White);
	title_.setPosition(0, 0);

	currentValue_.setFont(font_);
	currentValue_.setCharacterSize(13);
	currentValue_.setStyle(sf::Text::Regular);
	currentValue_.setFillColor(sf::Color::White);
	currentValue_.setPosition(1.7*radius_, 1.8*radius_);

	fillColorArray();

	texture_->create(2*radius_, 2*radius_);
	texture_->update(colorArray_);

	sprite_->setTexture(*texture_);
}

void ColorRepresentation::draw(sf::RenderTarget& target, sf::RenderStates states) const{
	states.transform *= getTransform();
	if(sprite_) target.draw(*sprite_, states);

	target.draw(title_, states);
	target.draw(currentValue_, states);
}

void ColorRepresentation::fillColorArray(){
	for(int pixelX=0; pixelX < 2*radius_; pixelX++){
		for(int pixelY=0; pixelY < 2*radius_; pixelY++){
			fillPixel(pixelX, pixelY);
		}
	}
}

void ColorRepresentation::fillPixel(int pixelX, int pixelY){
	float xVec = pixelX - static_cast<int>(radius_);
	float yVec = static_cast<int>(radius_) - pixelY;

	float angle = atan2(yVec, xVec);
	if(angle < 0) angle = 2*PI + fmod(angle, 2*PI);

	float distanceFromCenter = sqrt(xVec*xVec + yVec*yVec);
	sf::Color convertedToRGB = convertMeToRGB(angle, distanceFromCenter);
	
	if(distanceFromCenter <= radius_){
		colorArray_[4 * (pixelY * 2 * radius_ + pixelX) + 0] = convertedToRGB.r;
		colorArray_[4 * (pixelY * 2 * radius_ + pixelX) + 1] = convertedToRGB.g;
		colorArray_[4 * (pixelY * 2 * radius_ + pixelX) + 2] = convertedToRGB.b;
		colorArray_[4 * (pixelY * 2 * radius_ + pixelX) + 3] = 255;
	}
	else{
		for(int i=0; i<4; i++)
			colorArray_[4 * (pixelY * 2 * radius_ + pixelX) + i] = 0;
	}
}

ColorRepresentation::~ColorRepresentation(){
	if(!colorArray_) delete[] colorArray_;
	if(!texture_) delete texture_;
	if(!sprite_) delete sprite_;

	colorArray_ = NULL;
	texture_ = NULL;
	sprite_ = NULL;
}