#pragma once
#include <vector>
#include "SFML/Graphics.hpp"

class ColorRepresentation : public sf::Drawable, public sf::Transformable{
protected:
	const float PI;
	const float brink_;
	unsigned radius_;

	sf::Font font_;
	sf::Text title_;
	sf::Text currentValue_;

	sf::Uint8 *colorArray_;
	sf::Texture *texture_;
	sf::Sprite *sprite_;
	
	ColorRepresentation(unsigned radius);
	void initialise();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	void fillColorArray();
	void fillPixel(int pixelX, int pixelY);

	virtual sf::Color convertMeToRGB(float angle, float distanceFromCenter) const = 0;

	virtual ~ColorRepresentation();
};