#pragma once
#include "SFML/Graphics.hpp"

struct EventInfo{
	bool isClicked;
	bool isReleased;
	sf::Vector2f mousePos;
	EventInfo():isClicked(false),isReleased(false){
		mousePos.x = 0;
		mousePos.y = 0;
	}
};