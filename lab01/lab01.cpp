// Calosc mozna dowolnie edytowac wedle uznania. 

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <cmath>
#include "font.h"


class Menu : public sf::Drawable
{
 private:
  sf::Font font;
  sf::Text *text;
  sf::RectangleShape *rectangle;
  
  sf::Texture *colors_texture;
  sf::Sprite *colors_sprite;
  sf::Uint8 *colors_pixels;
  const unsigned int colors_size_x = 765;
  const unsigned int colors_size_y = 60;
  inline void draw_to_color_pixels(unsigned int x, unsigned int y,unsigned char r, unsigned char g, unsigned char b)
  {
   colors_pixels[4 * (y * colors_size_x + x) + 0] = r;
   colors_pixels[4 * (y * colors_size_x + x) + 1] = g;
   colors_pixels[4 * (y * colors_size_x + x) + 2] = b;
   colors_pixels[4 * (y * colors_size_x + x) + 3] = 255;
  }

  char keyPressed;
  sf::Color drawingColor;
  sf::RectangleShape *rectDrawingColor;
  sf::Color fillingColor;
  sf::RectangleShape *rectFillingColor;

 public:
  Menu(): drawingColor(sf::Color::Black), fillingColor(sf::Color::Transparent)
  {
   font.loadFromMemory(font_data, font_data_size);
   text = new sf::Text;
   text->setFont(font);
   text->setCharacterSize(12);
   text->setFillColor(sf::Color::White);

   rectangle = new sf::RectangleShape(sf::Vector2f(796, 536));
   rectangle->setFillColor(sf::Color::Transparent);
   rectangle->setOutlineColor(sf::Color::White);
   rectangle->setOutlineThickness(1.0f);
   rectangle->setPosition(2, 62);

   unsigned int x, y;
   colors_pixels = new sf::Uint8[colors_size_x * colors_size_y * 4];
   for (x = 0; x<255; x++)
    for (y = 0; y < 30; y++)
     {
      draw_to_color_pixels(x, y, x, 255, 0);
      draw_to_color_pixels(x+255, y, 255, 255-x, 0);
      draw_to_color_pixels(x + 510, y, 255, 0, x);
      draw_to_color_pixels(254 - x, y+30, 0, 255, 255-x);
      draw_to_color_pixels(509 - x, y + 30, 0, x, 255 );
      draw_to_color_pixels(764 - x, y + 30, 255-x, 0, 255);
     }

   colors_texture = new sf::Texture();
   colors_texture->create(colors_size_x, colors_size_y);
   colors_texture->update(colors_pixels);

   colors_sprite = new sf::Sprite();
   colors_sprite->setTexture(*colors_texture);
   colors_sprite->setPosition(1, 1);

   keyPressed = ' ';

   rectDrawingColor = new sf::RectangleShape(sf::Vector2f(32,28));
   rectDrawingColor->setPosition(sf::Vector2f(767, 2));
   rectDrawingColor->setFillColor(drawingColor);

   rectFillingColor = new sf::RectangleShape(sf::Vector2f(32,28));
   rectFillingColor->setPosition(sf::Vector2f(767, 32));
   rectFillingColor->setFillColor(fillingColor);
  }

  void outtextxy(sf::RenderTarget& target, float x, float y, const wchar_t *str) const
  {
   text->setPosition(x, y); 
   text->setString(str); 
   target.draw(*text);
  }

  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
  {
   outtextxy(target,5, 600, L"f - wybór koloru rysowania");
   outtextxy(target, 5, 615, L"b - wybór koloru wypełniania");
   outtextxy(target, 5, 630, L"l - rysowanie linii");

   outtextxy(target, 200, 600, L"r - rysowanie prostokąta");
   outtextxy(target, 200, 615, L"a - rysowanie wypełnionego prostokąta");
   outtextxy(target, 200, 630, L"c - rysowanie okręgu");

   outtextxy(target, 470, 600, L"w - zapis do pliku");
   outtextxy(target, 470, 615, L"o - odczyt z pliku");
   outtextxy(target, 470, 630, L"esc - wyjście");

   outtextxy(target, 650, 615, L"Aktualne:");

   text->setPosition(710, 615);
   text->setString(keyPressed);
   target.draw(*text);

   target.draw(*rectangle);
   target.draw(*colors_sprite);

   rectDrawingColor->setFillColor(drawingColor);
   rectFillingColor->setFillColor(fillingColor);
   target.draw(*rectDrawingColor);
   target.draw(*rectFillingColor);
  }

  void setCurrentKeyPressed(sf::Keyboard::Key currentKey){
    switch(currentKey){
      case sf::Keyboard::Key::F:
        keyPressed = 'f';
        break;
      case sf::Keyboard::Key::B:
        keyPressed = 'b';
        break;
      case sf::Keyboard::Key::L:
        keyPressed = 'l';
        break;
      case sf::Keyboard::Key::R:
        keyPressed = 'r';
        break;
      case sf::Keyboard::Key::A:
        keyPressed = 'a';
        break;
      case sf::Keyboard::Key::C:
        keyPressed = 'c';
        break;
      case sf::Keyboard::Key::W:
        keyPressed = 'w';
        break;
      case sf::Keyboard::Key::O:
        keyPressed = 'o';
        break;
      default:
        keyPressed = ' ';
        break;
    }
  }

  void setDrawingColor(const sf::Color& color){
    drawingColor = color;
  }

  void setFillingColor(const sf::Color& color){
    fillingColor = color;
  }

  const sf::Uint8* getColorPicker() const{
    return colors_pixels;
  }

  ~Menu(){
    delete text;
    delete rectangle;
  
    delete colors_texture;
    delete colors_sprite;
    delete colors_pixels;
    delete rectDrawingColor;
    delete rectFillingColor;
  }
};

void freeArrayOfShapes(std::vector<sf::Drawable*>& arrayOfShapes);

bool checkIfMouseIsInsideCanvas(const sf::Vector2f& mousePos);
bool checkIfMouseIsInsideColorPicker(const sf::Vector2f& mousePos);

sf::Color pickColor(const sf::Vector2f& mousePos, const Menu& menu);

sf::VertexArray drawLine(const sf::Vector2f& startingPos, const sf::Vector2f& endingPos, const sf::Color& startColor, const sf::Color& endColor);
sf::RectangleShape drawRectangle(const sf::Vector2f& startingPos, const sf::Vector2f& endingPos, const sf::Color& outlineColor, const sf::Color& fillColor = sf::Color::Transparent);
sf::CircleShape drawCircle(const sf::Vector2f& startingPos, const sf::Vector2f& endingPos, const sf::Color& outlineColor, const sf::Color& fillColor = sf::Color::Transparent);

void saveToPng(const sf::RenderWindow& sourceWindow, const char *filename);
void openFromPng(sf::RenderWindow& targetWindow, const char *filename, std::vector<sf::Drawable*>& arrayOfShapes, sf::Texture& texture);

int main(){
  sf::RenderWindow window(sf::VideoMode(800, 650), "GFK Lab 01", sf::Style::Titlebar | sf::Style::Close);
  sf::Event event;
  Menu menu;
 
  window.setFramerateLimit(60);

  const char *filename = "image.png";

  bool isClickedOnCanvas = false;
  sf::Vector2f clickingPos;

  bool isClickedOnColorPicker = false;
  sf::Color drawingColor(sf::Color::Black);
  sf::Color fillingColor(sf::Color::Transparent);

  bool isReleased = false;
  sf::Vector2f currentPos;

  bool isKeyPressed = false;
  sf::Keyboard::Key keyPressed;

  sf::Texture openedImage;

  std::vector<sf::Drawable*> arrayOfShapes;

  while (window.isOpen()){
    window.clear(sf::Color::Black);
   
    while (window.pollEvent(event)){
      switch(event.type){
        case sf::Event::Closed:
          window.close();
          break;
        case sf::Event::KeyPressed:
          isKeyPressed = true;
          isClickedOnCanvas = isReleased = false;

          keyPressed = event.key.code;

          menu.setCurrentKeyPressed(keyPressed);
          break;
        case sf::Event::MouseButtonPressed:
          clickingPos.x = event.mouseButton.x;
          clickingPos.y = event.mouseButton.y;
          checkIfMouseIsInsideCanvas(clickingPos) ? isClickedOnCanvas = true : isClickedOnCanvas = false;
          checkIfMouseIsInsideColorPicker(clickingPos) ? isClickedOnColorPicker = true : isClickedOnColorPicker = false;
          break;
        case sf::Event::MouseButtonReleased:
          isReleased = true;
          break;
        case sf::Event::MouseMoved:
          if(checkIfMouseIsInsideCanvas(sf::Vector2f(event.mouseMove.x, event.mouseMove.y))){
            currentPos.x = event.mouseMove.x;
            currentPos.y = event.mouseMove.y;
          }
          break;
      }
    }

    for(int i=0; i<arrayOfShapes.size(); i++){
      window.draw(arrayOfShapes[i][0]);
    }

    if(isKeyPressed){
      if(isClickedOnCanvas){
        switch(keyPressed){
          case sf::Keyboard::Key::L:{
            sf::VertexArray line = drawLine(clickingPos, currentPos, drawingColor, fillingColor);
            window.draw(line);
            if(isReleased)  arrayOfShapes.push_back(new sf::VertexArray(line));
            break;
          }
          case sf::Keyboard::Key::R:{
            sf::RectangleShape rectangle = drawRectangle(clickingPos, currentPos, drawingColor);
            window.draw(rectangle);
            if(isReleased)  arrayOfShapes.push_back(new sf::RectangleShape(rectangle));
            break;
          }
          case sf::Keyboard::Key::A:{
            sf::RectangleShape rectangle = drawRectangle(clickingPos, currentPos, drawingColor, fillingColor);
            window.draw(rectangle);
            if(isReleased)  arrayOfShapes.push_back(new sf::RectangleShape(rectangle));
            break;
          }
          case sf::Keyboard::Key::C:{
            sf::CircleShape circle = drawCircle(clickingPos, currentPos, drawingColor);
            window.draw(circle);
            if(isReleased)  arrayOfShapes.push_back(new sf::CircleShape(circle));
            break;
          }
        }
        if(isReleased)  isClickedOnCanvas = isReleased = false;
      }
      if(isClickedOnColorPicker){
        switch(keyPressed){
          case sf::Keyboard::Key::F:{
            drawingColor = pickColor(clickingPos, menu);
            menu.setDrawingColor(drawingColor);
            break;
          }
          case sf::Keyboard::Key::B:{
            fillingColor = pickColor(clickingPos, menu);
            menu.setFillingColor(fillingColor);
            break;
          }
        }
        isClickedOnColorPicker = false;
      } 
    }

    window.draw(menu);
    window.display();

    if(isKeyPressed){
      if(keyPressed == sf::Keyboard::Key::W){
        saveToPng(window, filename);
        isKeyPressed = false;
        menu.setCurrentKeyPressed(sf::Keyboard::Key::Space);
      }
      if(keyPressed == sf::Keyboard::Key::O){
        openFromPng(window, filename, arrayOfShapes, openedImage);
        isKeyPressed = false;
        menu.setCurrentKeyPressed(sf::Keyboard::Key::Space);
      }
      if(keyPressed == sf::Keyboard::Key::Escape){
        window.close();
      }

    }
  }

  freeArrayOfShapes(arrayOfShapes);

  return 0;
}

void freeArrayOfShapes(std::vector<sf::Drawable*>& arrayOfShapes){
  for(int i=0; i<arrayOfShapes.size(); i++){
     delete arrayOfShapes[i];
  }
  arrayOfShapes.clear();
}

bool checkIfMouseIsInsideCanvas(const sf::Vector2f& mousePos){
  if(mousePos.x < 2 || mousePos.x > 798)  return false;
  if(mousePos.y < 62 || mousePos.y > 598) return false;
  return true;
}

bool checkIfMouseIsInsideColorPicker(const sf::Vector2f& mousePos){
  if(mousePos.x > 765)  return false;
  if(mousePos.y > 60) return false;
  return true;
}

sf::Color pickColor(const sf::Vector2f& mousePos, const Menu& menu){
  const sf::Uint8 *colorPicker = menu.getColorPicker();
  
  sf::Vector2i mP(static_cast<int>(mousePos.x), static_cast<int>(mousePos.y));
  unsigned char r = colorPicker[4 * (mP.y * 765 + mP.x) + 0];
  unsigned char g = colorPicker[4 * (mP.y * 765 + mP.x) + 1];
  unsigned char b = colorPicker[4 * (mP.y * 765 + mP.x) + 2];
  
  return sf::Color(r, g, b);
}

sf::VertexArray drawLine(const sf::Vector2f& startingPos, const sf::Vector2f& endingPos, const sf::Color& startColor, const sf::Color& endColor){
  sf::Vertex start(startingPos, startColor);
  sf::Vertex end(endingPos, endColor);
  sf::VertexArray line(sf::Lines, 2);

  line[0] = start;
  line[1] = end;
  return line;
}

sf::RectangleShape drawRectangle(const sf::Vector2f& startingPos, const sf::Vector2f& endingPos, const sf::Color& outlineColor, const sf::Color& fillColor){
  sf::RectangleShape rectangle(endingPos - startingPos);

  rectangle.setPosition(startingPos);
  rectangle.setOutlineColor(outlineColor);
  rectangle.setOutlineThickness(1);
  rectangle.setFillColor(fillColor);
  return rectangle;
}

sf::CircleShape drawCircle(const sf::Vector2f& startingPos, const sf::Vector2f& endingPos, const sf::Color& outlineColor, const sf::Color& fillColor){
  const float PI = 4*atan(1);
  sf::CircleShape circle;
  float radius = sqrt(pow(endingPos.x-startingPos.x,2)+pow(endingPos.y-startingPos.y,2))/2;
  float rotatingAngle = atan2(endingPos.y-startingPos.y, endingPos.x-startingPos.x)*(180/PI)-45;

  circle.setPosition(startingPos);
  circle.setRadius(sqrt(pow(endingPos.x-startingPos.x,2)+pow(endingPos.y-startingPos.y,2))/2);
  circle.rotate(rotatingAngle);
  circle.move(sf::Vector2f((radius-radius*sqrt(2))*cos((rotatingAngle+45)*PI/180), (radius-radius*sqrt(2))*sin((rotatingAngle+45)*PI/180)));
  circle.setOutlineColor(outlineColor);
  circle.setOutlineThickness(1);
  circle.setFillColor(fillColor);
  return circle;
}

void saveToPng(const sf::RenderWindow& sourceWindow, const char *filename){
  sf::Vector2u windowSize = sourceWindow.getSize();
  sf::Texture texture;
  texture.create(windowSize.x, windowSize.y);
  texture.update(sourceWindow);
  sf::Image wholeWindow = texture.copyToImage();

  sf::Image canvasImage;
  canvasImage.create(796, 536, sf::Color::Black);
  canvasImage.copy(wholeWindow, 0, 0, sf::IntRect(2, 62, 796, 536));
  canvasImage.saveToFile(filename);
}

void openFromPng(sf::RenderWindow& sourceWindow, const char *filename, std::vector<sf::Drawable*>& arrayOfShapes, sf::Texture& texture){
  sf::Image imageFromFile;
  if(!imageFromFile.loadFromFile(filename)) return;
  freeArrayOfShapes(arrayOfShapes);
  texture.loadFromImage(imageFromFile);
  sf::Sprite sprite(texture);
  sprite.setPosition(sf::Vector2f(2, 62));
  arrayOfShapes.push_back(new sf::Sprite(sprite));
}