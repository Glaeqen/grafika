#pragma once

#include <wx/wx.h>
#include <libgfl.h>
#include <libgfle.h>

class Frame : public wxFrame {
    wxPanel *picturePanel;
    wxButton *importPictureButton;
    wxButton *gaussMaskButton;
    wxButton *sepiaFilterButton;
    wxButton *balanceHistogramButton;

	wxButton *blurButton;

    wxButton *savePictureButton;
    wxTextCtrl *exifTextCtrl;

    wxBoxSizer *mainSizer;
	wxBoxSizer *pictureSizer;
    wxBoxSizer *buttonSizer;

    wxImage *image;
	wxImage *imageCpy;
	GFL_BITMAP *gflBitmap;

    void onClose(wxCloseEvent &event);

	void refreshUI();

    void onUpdateUI(wxUpdateUIEvent &event);

    void CreateGUIControls();

    void onImportPictureButtonClick(wxCommandEvent &event);

    void onGaussMaskButtonClick(wxCommandEvent &event);

    void onSepiaFilterButtonClick(wxCommandEvent &event);

    void onBalanceHistogramButtonClick(wxCommandEvent &event);

	void onBlurButtonClick(wxCommandEvent &event);

    void onSavePictureButtonClick(wxCommandEvent &event);

public:
    Frame(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Lab_06"),
          const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize,
          long style = wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX);

    virtual ~Frame();
};
