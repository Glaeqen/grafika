#include "Frame.h"

#include <CImg.h>
#include <wx/dcbuffer.h>

using namespace cimg_library;

Frame::Frame(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize &size,
             long style)
        : wxFrame(parent, id, title, position, size, style) {
    gflLibraryInit();
    CreateGUIControls();
}

Frame::~Frame() {
}

void Frame::CreateGUIControls() {

    mainSizer = new wxBoxSizer(wxHORIZONTAL);
    this->SetSizer(mainSizer);
    this->SetAutoLayout(true);

    buttonSizer = new wxBoxSizer(wxVERTICAL);
    mainSizer->Add(buttonSizer, 0, wxTOP | wxEXPAND);

    importPictureButton = new wxButton(this, wxID_ANY, wxT("Zaimportuj obraz"), wxDefaultPosition, wxSize(150, 25));
    buttonSizer->Add(importPictureButton, 0, wxALIGN_CENTER | wxALL, 5);

    gaussMaskButton = new wxButton(this, wxID_ANY, wxT("Naloz maske Gaussa"), wxDefaultPosition, wxSize(150, 25));
    buttonSizer->Add(gaussMaskButton, 0, wxALIGN_CENTER | wxALL, 5);

    sepiaFilterButton = new wxButton(this, wxID_ANY, wxT("Naloz filter sepii"), wxDefaultPosition, wxSize(150, 25));
    buttonSizer->Add(sepiaFilterButton, 0, wxALIGN_CENTER | wxALL, 5);

    balanceHistogramButton = new wxButton(this, wxID_ANY, wxT("Wyrownaj histogram"), wxDefaultPosition,
                                          wxSize(150,25));
    buttonSizer->Add(balanceHistogramButton, 0, wxALIGN_CENTER | wxALL, 5);

	blurButton = new wxButton(this, wxID_ANY, wxT("Naloz efekt Blur"), wxDefaultPosition,
		wxSize(150, 25));
	buttonSizer->Add(blurButton, 0, wxALIGN_CENTER | wxALL, 5);

    savePictureButton = new wxButton(this, wxID_ANY, wxT("Zapisz obrazek"), wxDefaultPosition, wxSize(150, 25));
    buttonSizer->Add(savePictureButton, 0, wxALIGN_CENTER | wxALL, 5);

    exifTextCtrl = new wxTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(250, 0),
                                  wxTE_READONLY | wxTE_MULTILINE | wxVSCROLL);
    buttonSizer->Add(exifTextCtrl, 1, wxEXPAND | wxALL, 5);

	pictureSizer = new wxBoxSizer(wxVERTICAL);
	mainSizer->Add(pictureSizer, 1, wxEXPAND | wxALL, 5);
    picturePanel = new wxPanel(this, wxID_ANY, wxDefaultPosition, wxSize(500, 500));
    picturePanel->SetBackgroundColour(*wxWHITE);
    pictureSizer->Add(picturePanel, 1, wxEXPAND | wxALL);

    SetTitle(wxT("Lab 08"));
    SetIcon(wxNullIcon);

    GetSizer()->Layout();
    GetSizer()->Fit(this);
    GetSizer()->SetSizeHints(this);
    Center();

    image = new wxImage;

    importPictureButton->Bind(wxEVT_BUTTON, &Frame::onImportPictureButtonClick, this);
    gaussMaskButton->Bind(wxEVT_BUTTON, &Frame::onGaussMaskButtonClick, this);
    sepiaFilterButton->Bind(wxEVT_BUTTON, &Frame::onSepiaFilterButtonClick, this);
    balanceHistogramButton->Bind(wxEVT_BUTTON, &Frame::onBalanceHistogramButtonClick, this);
	blurButton->Bind(wxEVT_BUTTON, &Frame::onBlurButtonClick, this);
    savePictureButton->Bind(wxEVT_BUTTON, &Frame::onSavePictureButtonClick, this);

    Bind(wxEVT_UPDATE_UI, &Frame::onUpdateUI, this);
    Bind(wxEVT_CLOSE_WINDOW, &Frame::onClose, this);
}

void Frame::onClose(wxCloseEvent &event) {
    Destroy();
}

void Frame::refreshUI() {
	if (image->Ok()) {
		wxClientDC dc(picturePanel);
		wxBufferedDC bufferedDC(&dc);
		wxSize size(dc.GetSize());

		wxImage resizedImage(*image);
		resizedImage.Rescale(size.x, size.y);
		wxBitmap bitmap(resizedImage);
		bufferedDC.DrawBitmap(bitmap, 0, 0);
	}
}

void Frame::onUpdateUI(wxUpdateUIEvent &event) {
	refreshUI();
}

void Frame::onImportPictureButtonClick(wxCommandEvent &event) {
    wxFileDialog fileDialog(this, "Otw�rz zdj�cie", "", "", "(*.jpg)|*.jpg", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (fileDialog.ShowModal() == wxID_CANCEL) return;

    wxString path = fileDialog.GetPath();
    GFL_FILE_INFORMATION fileInformation;
    GFL_LOAD_PARAMS loadParams;
    GFL_COLOR color;

    gflGetDefaultLoadParams(&loadParams);
    loadParams.Flags |= GFL_LOAD_METADATA;

    GFL_ERROR error = gflLoadBitmap(path, &gflBitmap, &loadParams, &fileInformation);

    if(error){
        std::cout << "Error gflLoadBitmap occurred. Code: " << error << std::endl;
        return;
    }

    image->Create(gflBitmap->Width, gflBitmap->Height);
    for(int i=0; i<gflBitmap->Width; i++){
        for(int j=0; j<gflBitmap->Height; j++){
            gflGetColorAt(gflBitmap, i, j, &color);
            image->SetRGB(i, j, color.Red, color.Green, color.Blue);
        }
    }

	exifTextCtrl->Clear();
	GFL_EXIF_DATA *exifData;
	if (!gflBitmapHasEXIF(gflBitmap)) {
		wxMessageDialog(this, "No EXIF Found", "No EXIF Found", wxOK).ShowModal();
		return;
	}
	exifData = gflBitmapGetEXIF(gflBitmap, GFL_EXIF_WANT_MAKERNOTES);
	exifTextCtrl->AppendText(wxString(exifData->ItemsList[0].Name) + ":\n\n");
	for (int i = 1; i < exifData->NumberOfItems; i++) {
		wxString currentData = wxString(exifData->ItemsList[i].Name) + " : "  + exifData->ItemsList[i].Value + '\n';
		exifTextCtrl->AppendText(currentData);
	}

}

void Frame::onGaussMaskButtonClick(wxCommandEvent &event) {
	if (!image->IsOk()) return;
	CImg<unsigned char> cImage(image->GetWidth(), image->GetHeight(), 1, 3);
	CImg<float> g_mask(image->GetWidth(), image->GetHeight(), 1, 1);
	float c[] = { 1.0 };
	g_mask.draw_gaussian(image->GetWidth() / 2.0, image->GetHeight() / 2.0, 255.0, c);
	for (int i = 0; i<image->GetWidth(); ++i)
		for (int j = 0; j<image->GetHeight(); ++j) {
			cImage(i, j, 0) = image->GetRed(i, j);
			cImage(i, j, 1) = image->GetGreen(i, j);
			cImage(i, j, 2) = image->GetBlue(i, j);
		}
	cImage.mul(g_mask);
	int r, g, b;
	for (int i = 0; i<image->GetWidth(); ++i)
		for (int j = 0; j<image->GetHeight(); ++j) {
			r = cImage(i, j, 0) > 255.0 ? 255 : int(cImage(i, j, 0) / 255.0f*image->GetRed(i, j));
			g = cImage(i, j, 1) > 255.0 ? 255 : int(cImage(i, j, 0) / 255.0f*image->GetGreen(i, j));
			b = cImage(i, j, 2) > 255.0 ? 255 : int(cImage(i, j, 0) / 255.0f*image->GetBlue(i, j));
			image->SetRGB(i, j, r, g, b);
		}
}

void Frame::onSepiaFilterButtonClick(wxCommandEvent &event) {
	GFL_COLOR color;
	gflSepia(gflBitmap, NULL, 80);
	for (int i = 0; i<gflBitmap->Width; ++i)
		for (int j = 0; j<gflBitmap->Height; ++j) {
			gflGetColorAt(gflBitmap, i, j, &color);
			image->SetRGB(i, j, color.Red, color.Green, color.Blue);
		}
}

void Frame::onBalanceHistogramButtonClick(wxCommandEvent &event) {
	if (!image->IsOk()) return;
	CImg<unsigned char> cImage(image->GetWidth(), image->GetHeight(), 1, 3);
	for (int i = 0; i<image->GetWidth(); ++i)
		for (int j = 0; j<image->GetHeight(); ++j) {
			cImage(i, j, 0) = image->GetRed(i, j);
			cImage(i, j, 1) = image->GetGreen(i, j);
			cImage(i, j, 2) = image->GetBlue(i, j);
		}
	cImage.equalize(256);
	for (int i = 0; i<image->GetWidth(); ++i)
		for (int j = 0; j<image->GetHeight(); ++j) {
			image->SetRGB(i, j, cImage(i, j, 0), cImage(i, j, 1), cImage(i, j, 2));
		}
}

void Frame::onBlurButtonClick(wxCommandEvent &event) {
	if (!image->IsOk()) return;
	CImg<unsigned char> cImage(image->GetWidth(), image->GetHeight(), 1, 3);
	for (int i = 0; i<image->GetWidth(); ++i)
		for (int j = 0; j<image->GetHeight(); ++j) {
			cImage(i, j, 0) = image->GetRed(i, j);
			cImage(i, j, 1) = image->GetGreen(i, j);
			cImage(i, j, 2) = image->GetBlue(i, j);
		}
	cImage.blur(10);
	for (int i = 0; i<image->GetWidth(); ++i)
		for (int j = 0; j<image->GetHeight(); ++j) {
			image->SetRGB(i, j, cImage(i, j, 0), cImage(i, j, 1), cImage(i, j, 2));
		}
}

void Frame::onSavePictureButtonClick(wxCommandEvent &event) {
    wxFileDialog fileDialog(this, "Zapisz zdj�cie", "", "", "Do PNG (*.png)|*.png", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);

	if (fileDialog.ShowModal() == wxID_CANCEL)	return;

	wxImage::AddHandler(new wxPNGHandler);
	image->SaveFile(fileDialog.GetPath(), wxBITMAP_TYPE_PNG);
}
