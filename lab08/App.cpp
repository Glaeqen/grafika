#include "App.h"
#include "Frame.h"

IMPLEMENT_APP(App)

bool App::OnInit()
{
    Frame* frame = new Frame(NULL);
    SetTopWindow(frame);
    frame->Show();
    return true;
}

int App::OnExit()
{
	return 0;
}
