/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 7              */
/* Graficzna prezentacja danych                              J. Tarasiuk 2008 */
/******************************************************************************/

// UWAGA: T E G O   P L I K U   N I E   Z M I E N I A M Y  ! ! ! ***************

#include <vector>
#include "Main_Frm.h"

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End
#define wxT

//----------------------------------------------------------------------------
// Main_Frm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(Main_Frm, wxFrame)
                ////Manual Code Start
                ////Manual Code End

                EVT_CLOSE(Main_Frm::OnClose)
                EVT_TOGGLEBUTTON(ID_WXTOGGLEBUTTON5, Main_Frm::WxToggleButton5Click)

                EVT_COMMAND_SCROLL(ID_WXSCROLLBAR1, Main_Frm::WxScrollBar1Scroll)
                EVT_COMMAND_SCROLL_THUMBRELEASE(ID_WXSCROLLBAR1, Main_Frm::WxScrollBar1ScrollThumbRelease)
                EVT_TOGGLEBUTTON(ID_WXTOGGLEBUTTON4, Main_Frm::WxToggleButton4Click)
                EVT_TOGGLEBUTTON(ID_WXTOGGLEBUTTON3, Main_Frm::WxToggleButton3Click)
                EVT_TOGGLEBUTTON(ID_WXTOGGLEBUTTON2, Main_Frm::WxToggleButton2Click)
                EVT_TOGGLEBUTTON(ID_WXTOGGLEBUTTON1, Main_Frm::WxToggleButton1Click)
                EVT_RADIOBUTTON(ID_WXRADIOBUTTON3, Main_Frm::WxRadioButton3Click)
                EVT_RADIOBUTTON(ID_WXRADIOBUTTON2, Main_Frm::WxRadioButton2Click)
                EVT_RADIOBUTTON(ID_WXRADIOBUTTON1, Main_Frm::WxRadioButton1Click)
                EVT_CHECKBOX(ID_CB_MAPA, Main_Frm::WxCheckBox1Click)
                EVT_CHECKBOX(ID_WXCHECKBOX2, Main_Frm::WxCheckBox2Click)

                EVT_UPDATE_UI(ID_WXSCROLLEDWINDOW1, Main_Frm::WxScrolledWindow1UpdateUI)
END_EVENT_TABLE()
////Event Table End

float FunctionData[100][3];
int NoLevels = 5, ColoringType = 0, NoPoints = 4;
bool ContourFlag = false;
bool ShowPoints = false;

wxString Float2String(float f, int numdigits) {
    wxString Result;
    if (numdigits == 0) {
        Result = Result.Format("%f", f);
    } else {
        wxString strNumFormat = wxString::Format("%%0.%df", numdigits);
        Result = Result.Format(strNumFormat, f);
    }
    return Result;
}

void PrepareData(int fun) {
    int i;
    float x, y;
    srand(376257);
    switch (fun) {
        case 0:
            NoPoints = 5;
            FunctionData[0][0] = -1.0;
            FunctionData[0][1] = 1.0;
            FunctionData[0][2] = 0.0;
            FunctionData[1][0] = 1.0;
            FunctionData[1][1] = 1.0;
            FunctionData[1][2] = 25.0;
            FunctionData[2][0] = 1.0;
            FunctionData[2][1] = -1.0;
            FunctionData[2][2] = 5.0;
            FunctionData[3][0] = -1.0;
            FunctionData[3][1] = -1.0;
            FunctionData[3][2] = 25.0;
            FunctionData[4][0] = 0.0;
            FunctionData[4][1] = 0.0;
            FunctionData[4][2] = 15.0;
            break;
        case 1:
            NoPoints = 20;
            for (i = 0; i < NoPoints; i++) {
                x = 4.8 * (float(rand()) / RAND_MAX) - 2.4;
                y = 4.8 * (float(rand()) / RAND_MAX) - 2.4;
                FunctionData[i][0] = x;
                FunctionData[i][1] = y;
                FunctionData[i][2] = 200 * (float(rand()) / RAND_MAX);
            }
            break;
        case 2:
            NoPoints = 100;
            for (i = 0; i < NoPoints; i++) {
                x = 4.8 * (float(rand()) / RAND_MAX) - 2.4;
                y = 4.8 * (float(rand()) / RAND_MAX) - 2.4;
                FunctionData[i][0] = x;
                FunctionData[i][1] = y;
                FunctionData[i][2] = x * (2 * x - 7) * (2 * y + 1) + 2 * y;
            }
            break;
        case 3:
            NoPoints = 10;
            for (i = 0; i < NoPoints; i++) {
                x = 4.8 * (float(rand()) / RAND_MAX) - 2.4;
                y = 4.8 * (float(rand()) / RAND_MAX) - 2.4;
                FunctionData[i][0] = x;
                FunctionData[i][1] = y;
                FunctionData[i][2] = x * sin(2 * y);
            }
            break;
        case 4:
            NoPoints = 100;
            for (i = 0; i < NoPoints; i++) {
                x = 2 * (float(rand()) / RAND_MAX) - 1;
                y = 2 * (float(rand()) / RAND_MAX) - 1;
                FunctionData[i][0] = x;
                FunctionData[i][1] = y;
                FunctionData[i][2] = sin(8 * (x * x + y * y));
            }
            break;
    }
}

Main_Frm::Main_Frm(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize &size,
                   long style)
        : wxFrame(parent, id, title, position, size, style) {
    CreateGUIControls();
//	this->SetBackgroundColour(wxColour(192,192,192));
    MemoryBitmap.Create(500, 500);
    PrepareData(0);
}

Main_Frm::~Main_Frm() {
}

void Main_Frm::CreateGUIControls() {
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    WxBoxSizer1 = new wxBoxSizer(wxVERTICAL);
    this->SetSizer(WxBoxSizer1);
    this->SetAutoLayout(true);

    WxScrolledWindow1 = new wxScrolledWindow(this, ID_WXSCROLLEDWINDOW1, wxPoint(5, 5), wxSize(500, 500),
                                             wxVSCROLL | wxHSCROLL);
    WxScrolledWindow1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer1->Add(WxScrolledWindow1, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer1->Add(WxBoxSizer2, 0, wxALIGN_CENTER | wxALIGN_CENTER_VERTICAL | wxALL, 5);

    wxStaticBox *WxStaticBoxSizer1_StaticBoxObj = new wxStaticBox(this, wxID_ANY, wxT(" Tryb rysowania "));
    WxStaticBoxSizer1_StaticBoxObj->SetFont(wxFont(9, wxSWISS, wxNORMAL, wxNORMAL));
    WxStaticBoxSizer1 = new wxStaticBoxSizer(WxStaticBoxSizer1_StaticBoxObj, wxHORIZONTAL);
    WxBoxSizer2->Add(WxStaticBoxSizer1, 0, wxALIGN_CENTER | wxALL, 5);

    WxCheckBox2 = new wxCheckBox(this, ID_WXCHECKBOX2, wxT("Kontur"), wxPoint(10, 20), wxSize(62, 17), 0,
                                 wxDefaultValidator, wxT("WxCheckBox2"));
    WxCheckBox2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer1->Add(WxCheckBox2, 0, wxALIGN_CENTER | wxALL, 5);

    CB_Mapa = new wxCheckBox(this, ID_CB_MAPA, wxT("Mapa"), wxPoint(10, 20), wxSize(55, 17), 0, wxDefaultValidator, wxT
                             ("CB_Mapa"));
    CB_Mapa->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer1->Add(CB_Mapa, 0, wxALIGN_CENTER | wxALL, 5);

    WxRadioButton1 = new wxRadioButton(this, ID_WXRADIOBUTTON1, wxT("N/C"), wxPoint(10, 20), wxSize(50, 17), 0,
                                       wxDefaultValidator, wxT("WxRadioButton1"));
    WxRadioButton1->Enable(false);
    WxRadioButton1->SetValue(true);
    WxRadioButton1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer1->Add(WxRadioButton1, 0, wxALIGN_CENTER | wxALL, 5);

    WxRadioButton2 = new wxRadioButton(this, ID_WXRADIOBUTTON2, wxT("N/Z/C"), wxPoint(10, 20), wxSize(60, 17), 0,
                                       wxDefaultValidator, wxT("WxRadioButton2"));
    WxRadioButton2->Enable(false);
    WxRadioButton2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer1->Add(WxRadioButton2, 0, wxALIGN_CENTER | wxALL, 5);

    WxRadioButton3 = new wxRadioButton(this, ID_WXRADIOBUTTON3, wxT("Na szaro"), wxPoint(10, 20), wxSize(80, 17), 0,
                                       wxDefaultValidator, wxT("WxRadioButton3"));
    WxRadioButton3->Enable(false);
    WxRadioButton3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer1->Add(WxRadioButton3, 0, wxALIGN_CENTER | wxALL, 5);

    wxStaticBox *WxStaticBoxSizer2_StaticBoxObj = new wxStaticBox(this, wxID_ANY, wxT(" Wybor funkcji "));
    WxStaticBoxSizer2_StaticBoxObj->SetFont(wxFont(9, wxSWISS, wxNORMAL, wxNORMAL));
    WxStaticBoxSizer2 = new wxStaticBoxSizer(WxStaticBoxSizer2_StaticBoxObj, wxHORIZONTAL);
    WxBoxSizer2->Add(WxStaticBoxSizer2, 0, wxALIGN_CENTER | wxALL, 5);

    WxToggleButton1 = new wxToggleButton(this, ID_WXTOGGLEBUTTON1, wxT("1"), wxPoint(10, 20), wxSize(25, 25), 0,
                                         wxDefaultValidator, wxT("WxToggleButton1"));
    WxToggleButton1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer2->Add(WxToggleButton1, 0, wxALIGN_CENTER | wxALL, 5);

    WxToggleButton2 = new wxToggleButton(this, ID_WXTOGGLEBUTTON2, wxT("2"), wxPoint(45, 20), wxSize(25, 25), 0,
                                         wxDefaultValidator, wxT("WxToggleButton2"));
    WxToggleButton2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer2->Add(WxToggleButton2, 0, wxALIGN_CENTER | wxALL, 5);

    WxToggleButton3 = new wxToggleButton(this, ID_WXTOGGLEBUTTON3, wxT("3"), wxPoint(80, 20), wxSize(25, 25), 0,
                                         wxDefaultValidator, wxT("WxToggleButton3"));
    WxToggleButton3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer2->Add(WxToggleButton3, 0, wxALIGN_CENTER | wxALL, 5);

    WxToggleButton4 = new wxToggleButton(this, ID_WXTOGGLEBUTTON4, wxT("4"), wxPoint(115, 20), wxSize(25, 25), 0,
                                         wxDefaultValidator, wxT("WxToggleButton4"));
    WxToggleButton4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxStaticBoxSizer2->Add(WxToggleButton4, 0, wxALIGN_CENTER | wxALL, 5);

    WxFlexGridSizer1 = new wxFlexGridSizer(0, 3, 0, 0);
    WxBoxSizer1->Add(WxFlexGridSizer1, 0, wxALIGN_CENTER | wxALL, 5);

    WxScrollBar1 = new wxScrollBar(this, ID_WXSCROLLBAR1, wxPoint(5, 9), wxSize(250, 16), wxSB_HORIZONTAL,
                                   wxDefaultValidator, wxT("WxScrollBar1"));
    WxScrollBar1->Enable(false);
    WxScrollBar1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxFlexGridSizer1->Add(WxScrollBar1, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText1 = new wxStaticText(this, ID_WXSTATICTEXT1, wxT("Liczba poziomic: 5.0"), wxPoint(265, 9),
                                     wxDefaultSize, 0, wxT("WxStaticText1"));
    WxStaticText1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxFlexGridSizer1->Add(WxStaticText1, 0, wxALIGN_CENTER | wxALL, 5);

    WxToggleButton5 = new wxToggleButton(this, ID_WXTOGGLEBUTTON5, wxT("Pokaz punkty"), wxPoint(374, 5),
                                         wxSize(100, 25), 0, wxDefaultValidator, wxT("WxToggleButton5"));
    WxToggleButton5->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxFlexGridSizer1->Add(WxToggleButton5, 0, wxALIGN_CENTER | wxALL, 5);

    SetTitle(wxT("Lab_07"));
    SetIcon(wxNullIcon);

    GetSizer()->Layout();
    GetSizer()->Fit(this);
    GetSizer()->SetSizeHints(this);
    Center();

    ////GUI Items Creation End
    WxScrollBar1->Enable(true);
    WxScrollBar1->SetScrollbar(5, 1, 9, 1);
}

void Main_Frm::OnClose(wxCloseEvent &event) {
    Destroy();
}

/*
 * WxCheckBox1Click
 */
void Main_Frm::WxCheckBox1Click(wxCommandEvent &event) {
    if (CB_Mapa->IsChecked()) {
        WxRadioButton1->Enable();
        WxRadioButton2->Enable();
        WxRadioButton3->Enable();
        if (WxRadioButton1->GetValue()) ColoringType = 1;
        if (WxRadioButton2->GetValue()) ColoringType = 2;
        if (WxRadioButton3->GetValue()) ColoringType = 3;
    } else {
        WxRadioButton1->Disable();
        WxRadioButton2->Disable();
        WxRadioButton3->Disable();
        WxCheckBox2->SetValue(true);
        ColoringType = 0;
        ContourFlag = true;
    }
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}


/*
 * WxCheckBox2Click
 */
void Main_Frm::WxCheckBox2Click(wxCommandEvent &event) {
    ContourFlag = WxCheckBox2->IsChecked();
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxToggleButton1Click
 */
void Main_Frm::WxToggleButton1Click(wxCommandEvent &event) {
    if (WxToggleButton1->GetValue()) PrepareData(1); else PrepareData(0);
    WxToggleButton2->SetValue(false);
    WxToggleButton3->SetValue(false);
    WxToggleButton4->SetValue(false);
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxToggleButton2Click
 */
void Main_Frm::WxToggleButton2Click(wxCommandEvent &event) {
    if (WxToggleButton2->GetValue()) PrepareData(2); else PrepareData(0);
    WxToggleButton1->SetValue(false);
    WxToggleButton3->SetValue(false);
    WxToggleButton4->SetValue(false);
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxToggleButton3Click
 */
void Main_Frm::WxToggleButton3Click(wxCommandEvent &event) {
    if (WxToggleButton3->GetValue()) PrepareData(3); else PrepareData(0);
    WxToggleButton1->SetValue(false);
    WxToggleButton2->SetValue(false);
    WxToggleButton4->SetValue(false);
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxToggleButton4Click
 */
void Main_Frm::WxToggleButton4Click(wxCommandEvent &event) {
    if (WxToggleButton4->GetValue()) PrepareData(4); else PrepareData(0);
    WxToggleButton1->SetValue(false);
    WxToggleButton2->SetValue(false);
    WxToggleButton3->SetValue(false);
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxScrolledWindow1UpdateUI
 */
void Main_Frm::WxScrolledWindow1UpdateUI(wxUpdateUIEvent &event) {
    wxMemoryDC memDC;
    memDC.SelectObject(MemoryBitmap);
    wxClientDC dc(WxScrolledWindow1);
    dc.Blit(0, 0, 500, 500, &memDC, 0, 0);
    memDC.SelectObject(wxNullBitmap);
}

/*
 * WxScrollBar1Scroll
 */
void Main_Frm::WxScrollBar1Scroll(wxScrollEvent &event) {
    wxString label = "Liczba poziomic: ";
    NoLevels = WxScrollBar1->GetThumbPosition() + 1;
    label = label + Float2String(NoLevels, 1);
    WxStaticText1->SetLabel(label);

    wxEventType eventType = event.GetEventType();
//    if (eventType==10115)
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxScrollBar1ScrollThumbRelease
 */
void Main_Frm::WxScrollBar1ScrollThumbRelease(wxScrollEvent &event) {
    wxMessageBox("aaaaaaa");  // DLACZEGO TO NIE DZIALA ? <<<<<<<<<<<<<<<<<<<<<<
}

/*
 * WxRadioButton1Click
 */
void Main_Frm::WxRadioButton1Click(wxCommandEvent &event) {
    ColoringType = 1;
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxRadioButton2Click
 */
void Main_Frm::WxRadioButton2Click(wxCommandEvent &event) {
    ColoringType = 2;
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxRadioButton3Click
 */
void Main_Frm::WxRadioButton3Click(wxCommandEvent &event) {
    ColoringType = 3;
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}

/*
 * WxToggleButton5Click
 */
void Main_Frm::WxToggleButton5Click(wxCommandEvent &event) {
    ShowPoints = WxToggleButton5->GetValue();
    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints);
}


#include "draw_map.cpp"


