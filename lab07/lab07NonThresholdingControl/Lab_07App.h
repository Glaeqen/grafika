//---------------------------------------------------------------------------
//
// Name:        Lab_07App.h
// Author:      Jacek
// Created:     2008-06-26 21:07:43
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __MAIN_FRMApp_h__
#define __MAIN_FRMApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class Main_FrmApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
