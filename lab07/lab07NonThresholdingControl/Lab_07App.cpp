/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 7              */
/* Graficzna prezentacja danych                              J. Tarasiuk 2008 */
/******************************************************************************/

// UWAGA: T E G O   P L I K U   N I E   Z M I E N I A M Y  ! ! ! ***************


#include "Lab_07App.h"
#include "Main_Frm.h"

IMPLEMENT_APP(Main_FrmApp)

bool Main_FrmApp::OnInit()
{
    Main_Frm* frame = new Main_Frm(NULL);
    SetTopWindow(frame);
    frame->Show();
    return true;
}
 
int Main_FrmApp::OnExit()
{
	return 0;
}
