/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 7              */
/* Graficzna prezentacja danych                              J. Tarasiuk 2008 */
/******************************************************************************/

// UWAGA: T E G O   P L I K U   N I E   Z M I E N I A M Y  ! ! ! ***************


#ifndef __MAIN_FRM_h__
#define __MAIN_FRM_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
    #include <wx/spinbutt.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/stattext.h>
#include <wx/scrolbar.h>
#include <wx/tglbtn.h>
#include <wx/radiobut.h>
#include <wx/checkbox.h>
#include <wx/scrolwin.h>
#include <wx/sizer.h>
////Header Include End

////Dialog Style Start
#undef Main_Frm_STYLE
#define Main_Frm_STYLE wxCAPTION | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class Main_Frm : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();

	public:
		Main_Frm(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Lab_07"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = Main_Frm_STYLE);
		virtual ~Main_Frm();
		void WxCheckBox1Click(wxCommandEvent& event);
		void WxCheckBox2Click(wxCommandEvent& event);
		void WxToggleButton1Click(wxCommandEvent& event);
		void WxToggleButton2Click(wxCommandEvent& event);
		void WxToggleButton3Click(wxCommandEvent& event);
		void WxToggleButton4Click(wxCommandEvent& event);
		void WxScrolledWindow1UpdateUI(wxUpdateUIEvent& event);
		void WxScrollBar2Scroll(wxScrollEvent& event);
		void WxScrollBar2ScrollEnd(wxScrollEvent& event);
		void WxScrollBar2ScrollThumbRelease(wxScrollEvent& event);
		void WxScrollBar2ScrollThumbtrack(wxScrollEvent& event);
		void WxScrollBar1Scroll(wxScrollEvent& event);
		void WxScrollBar1ScrollThumbRelease(wxScrollEvent& event);
		void WxRadioButton1Click(wxCommandEvent& event);
		void WxRadioButton2Click(wxCommandEvent& event);
		void WxRadioButton3Click(wxCommandEvent& event);
		void WxToggleButton5Click(wxCommandEvent& event);
        void WxSpinButtonEvent(wxSpinEvent& event);

	private:
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
        wxSpinButton *WxSpinButton;
        wxStaticText *WxStaticText2;
        wxToggleButton *WxToggleButton5;
		wxStaticText *WxStaticText1;
		wxScrollBar *WxScrollBar1;
		wxFlexGridSizer *WxFlexGridSizer1;
		wxToggleButton *WxToggleButton4;
		wxToggleButton *WxToggleButton3;
		wxToggleButton *WxToggleButton2;
		wxToggleButton *WxToggleButton1;
		wxStaticBoxSizer *WxStaticBoxSizer2;
		wxRadioButton *WxRadioButton3;
		wxRadioButton *WxRadioButton2;
		wxRadioButton *WxRadioButton1;
		wxCheckBox *CB_Mapa;
		wxCheckBox *WxCheckBox2;
		wxStaticBoxSizer *WxStaticBoxSizer1;
		wxBoxSizer *WxBoxSizer2;
		wxScrolledWindow *WxScrolledWindow1;
		wxBoxSizer *WxBoxSizer1;
		////GUI Control Declaration End

	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
            ID_WXSPINBUTTON = 1055,
			ID_WXTOGGLEBUTTON5 = 1041,
			ID_WXSTATICTEXT1 = 1040,
			ID_WXSCROLLBAR1 = 1039,
			ID_WXTOGGLEBUTTON4 = 1023,
			ID_WXTOGGLEBUTTON3 = 1022,
			ID_WXTOGGLEBUTTON2 = 1021,
			ID_WXTOGGLEBUTTON1 = 1020,
			ID_WXRADIOBUTTON3 = 1027,
			ID_WXRADIOBUTTON2 = 1026,
			ID_WXRADIOBUTTON1 = 1025,
			ID_CB_MAPA = 1024,
			ID_WXCHECKBOX2 = 1018,
			ID_WXSCROLLEDWINDOW1 = 1002,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();

	private:
        wxBitmap MemoryBitmap;
        void DrawMap(int N, float d[100][3], bool Contour, int MappingType, int NoLevels, bool ShowPoints, int Thresholding = 2);
};

#endif
