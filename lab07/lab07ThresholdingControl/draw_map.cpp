/******************************************************************************/
/* PODSTAWY GRAFIKI KOMPUTEROWEJ - Laboratorium Komputerowe nr 7              */
/* Graficzna prezentacja danych                              J. Tarasiuk 2008 */
/******************************************************************************/

// UWAGA: TO JEST JEDYNY PLIK, KTORY NALEZY EDYTOWAC **************************


wxPoint fromRealToBitmap(const wxRealPoint &realPoint) {
    // Real point: x:<-2.5, 2,5>; y:<-2,5, 2,5> -> Pixel x:<0, 500> y:<500, 0>
    int pixelX = static_cast<int>(100 * (realPoint.x + 2.5));
    int pixelY = static_cast<int>(100 * (2.5 - realPoint.y));
    return wxPoint(pixelX, pixelY);
}

wxRealPoint fromBitmapToReal(const wxPoint &pixel) {
    // Real point: x:<-2.5, 2,5>; y:<-2,5, 2,5> <- Pixel x:<0, 500> y:<500, 0>
    float realX = pixel.x / 100.0 - 2.5;
    float realY = -(pixel.y / 100.0 - 2.5);
    return wxRealPoint(realX, realY);
}

// Na pewno przyda sie jakas funkcja pomocnicza rysujaca kontury
void DrawContour(wxDC *dc, float shepardArray[500][500], int currentThreshold, int amountOfThresholds, float fMin,
                 float fMax, int probingRange = 2) {
    float thresholdPoint = (currentThreshold + 1) * (fMax - fMin) / (amountOfThresholds + 1.0f) + fMin;
    for (int w = 0; w < 500; w++) {
        for (int h = 0; h < 500; h++) {
            if (shepardArray[h][w] > thresholdPoint) {
                bool isFound = false;
                for(int i=-probingRange; i<probingRange+1; i++){
                    for(int j=-probingRange; j<probingRange+1; j++){
                        if(!i && !j) continue;
                        if(h+i < 0 || h+i >= 500) continue;
                        if(w+j < 0 || w+j >= 500) continue;
                        if(shepardArray[h+i][w+j] < thresholdPoint){
                            dc->DrawPoint(h, w);
                            isFound = true;
                            break;
                        }
                    }
                    if(isFound) break;
                }
            }
        }
    }
}

// Na pewno potrzebna bedzie funkcja wykonujaca interpolacje Sheparda
float Shepard(int N, float d[100][3], float x, float y) {
    const int p = 2;
    float sumW = 0.0;
    float sumWTimesZ = 0.0;
    // Check if point is already in an array
    for (int i = 0; i < N; i++) {
        if (fabs(d[i][0] - x) < 0.001)
            if (fabs(d[i][1] - y) < 0.001) {
                return d[i][2];
            }
        sumW += pow(sqrt((x - d[i][0]) * (x - d[i][0]) + (y - d[i][1]) * (y - d[i][1])), -p);
        sumWTimesZ += pow((x - d[i][0]) * (x - d[i][0]) + (y - d[i][1]) * (y - d[i][1]), -p / 2) * d[i][2];
    }
    // If not, then:
    return sumWTimesZ / sumW;
}

float Shepard(int N, float d[100][3], const wxRealPoint &realPoint) {
    return Shepard(N, d, realPoint.x, realPoint.y);
}

void Main_Frm::DrawMap(int N, float d[100][3], bool Contour, int MappingType, int NoLevels, bool ShowPoints, int Thresholding) {
    wxMemoryDC memDC;
    memDC.SelectObject(MemoryBitmap);
    memDC.SetBackground(*wxWHITE_BRUSH);
    memDC.Clear();
    if (!N) return;
    float shepardArray[500][500] = {{0.0}};
    for (int w = 0; w < MemoryBitmap.GetWidth(); w++) {
        for (int h = 0; h < MemoryBitmap.GetHeight(); h++) {
            shepardArray[h][w] = Shepard(N, d, fromBitmapToReal(wxPoint(h, w)));
        }
    }
    float fMin, fMax;
    fMin = fMax = d[0][2];
    for (int i = 1; i < N; i++) {
        if (d[i][2] < fMin) fMin = d[i][2];
        if (d[i][2] > fMax) fMax = d[i][2];
    }
    if (MappingType) {
        for (int w = 0; w < MemoryBitmap.GetWidth(); w++) {
            for (int h = 0; h < MemoryBitmap.GetHeight(); h++) {
                unsigned char colour =
                        static_cast<unsigned char>((shepardArray[h][w] - fMin) *
                                                   255 / (fMax - fMin));
                switch (MappingType) {
                    case 1:
                        memDC.SetPen(wxPen(wxColour(colour, 0, 255 - colour)));
                        break;
                    case 2:
                        memDC.SetPen(wxPen(wxColour((colour - 128) * 2 < 0 ? 0 : (colour - 128) * 2,
                                                    colour < 128 ? 2 * colour : -2 * colour + 2 * 255,
                                                    255 - 2 * colour < 0 ? 0 : 255 - 2 * colour)));
                        break;
                    case 3:
                        memDC.SetPen(wxPen(wxColour(colour, colour, colour)));
                        break;
                    default:
                        break;
                }
                memDC.DrawPoint(h, w);
            }
        }

    }
    if (Contour) {
        memDC.SetPen(*wxBLACK);
        for (int i = 0; i < NoLevels; i++) {
            DrawContour(&memDC, shepardArray, i, NoLevels, fMin, fMax, Thresholding);
        }
    }
    if (ShowPoints) {
        memDC.SetPen(*wxBLACK);
        memDC.SetBrush(*wxTRANSPARENT_BRUSH);
        for (int i = 0; i < N; i++) {
            wxPoint point = fromRealToBitmap(wxRealPoint(d[i][0], d[i][1]));
            wxPoint crossVLineStart(point.x, point.y - 3);
            wxPoint crossVLineEnd(point.x, point.y + 4);
            wxPoint crossHLineStart(point.x - 3, point.y);
            wxPoint crossHLineEnd(point.x + 4, point.y);

            memDC.DrawLine(crossVLineStart, crossVLineEnd);
            memDC.DrawLine(crossHLineStart, crossHLineEnd);
        }
    }
}

void Main_Frm::WxSpinButtonEvent(wxSpinEvent &event) {
    Thresholding = event.GetValue();
    WxStaticText2->SetLabel(std::string("Progowanie: ") + std::to_string(Thresholding));

    DrawMap(NoPoints, FunctionData, ContourFlag, ColoringType, NoLevels, ShowPoints, Thresholding);
}