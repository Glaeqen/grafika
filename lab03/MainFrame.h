#pragma once

#include <wx/wx.h>
#include <wx/image.h>
#include "Canvas.h"

class MainFrame : public wxFrame {
protected:
    wxSlider *carMovementSlider;
    wxCheckBox *lanternCheckBox;
    wxButton *lanternColorButton;
    wxTextCtrl *garageNameTextCtrl;
    wxChoice *carNameChoice;
    wxBitmapButton *saveToFileButton;
    Canvas *canvas;



public:
    MainFrame(wxWindow *parent, wxWindowID id = wxID_ANY, const wxString &title = wxT("Lab 03"),
              const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxSize(800, 600),
              long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

    ~MainFrame();
};