//
// Created by glaeqen on 26/03/17.
//

#include "Canvas.h"
#include <wx/graphics.h>
#include <wx/colordlg.h>
#include <wx/wfstream.h>
#include <iostream>

void Canvas::calculateCurrentCarPosition(int carSliderInput) {
    carPosition.x = int(250 - 1.8 * carSliderInput);
    carPosition.y = int(340 + 0.6 * carSliderInput);
}

Canvas::Canvas(wxWindow *parent, wxWindowID winid, const wxPoint &pos, const wxSize &size, long style,
               const wxString &name) : wxPanel(parent, winid, pos, size, style, name) {
    SetMinSize(size);
    SetMaxSize(size);
    SetBackgroundColour(wxColour(*wxWHITE));

    // Lantern
    lantern[0] = wxPoint(110, 110);
    lantern[1] = wxPoint(90, 120);
    lantern[2] = wxPoint(90, 330);
    lantern[3] = wxPoint(150, 360);
    lantern[4] = wxPoint(195, 310);

    // Default values
    currentCarPicked = 0;
    calculateCurrentCarPosition(50);
    isLanternOn = false;
    currentColourPicked = new wxColour(*wxYELLOW);
    //

    pngHandler = new wxPNGHandler();
    wxImage::AddHandler(pngHandler);

    foregroundImage = new wxImage(wxSize(GetSize().x, GetSize().y));
    foregroundImage->LoadFile("pictures/transparency.png", wxBITMAP_TYPE_PNG);
    backgroundImage = new wxImage(wxSize(GetSize().x, GetSize().y));
    backgroundImage->LoadFile("pictures/background.png", wxBITMAP_TYPE_PNG);

    cars[0] = new wxImage();
    cars[0]->LoadFile("pictures/Audi.png", wxBITMAP_TYPE_PNG);
    cars[1] = new wxImage();
    cars[1]->LoadFile("pictures/BMW.png", wxBITMAP_TYPE_PNG);

    Bind(wxEVT_PAINT, &Canvas::onPaint, this);
}

void Canvas::onPaint(wxPaintEvent &event) {
    wxPaintDC dc(this);

    wxGraphicsContext *gc = wxGraphicsContext::Create(dc);
    if (gc) {
        gc->DrawBitmap(wxBitmap(*backgroundImage), 0, 0, GetSize().x, GetSize().y);
        gc->DrawBitmap(wxBitmap(*cars[currentCarPicked]), carPosition.x, carPosition.y,
                       cars[currentCarPicked]->GetSize().x,
                       cars[currentCarPicked]->GetSize().y);
        gc->DrawBitmap(wxBitmap(*foregroundImage), 0, 0, GetSize().x, GetSize().y);
    }

    dc.SetTextForeground(wxColour(*wxBLACK));
    dc.DrawRotatedText(garageName, 270, 230, -17);
    dc.SetPen(wxPen(*currentColourPicked));
    dc.SetBrush(wxBrush(*currentColourPicked));
    if (isLanternOn) dc.DrawPolygon(5, lantern, wxWINDING_RULE);
}

void Canvas::onSliderMove(wxScrollEvent &event) {
    calculateCurrentCarPosition(event.GetPosition());
    Refresh();
}

void Canvas::onCheckboxChange(wxCommandEvent &event) {
    isLanternOn = event.IsChecked();
    Refresh();
}

void Canvas::onLanternColorButtonClicked(wxCommandEvent &event) {
    wxColourDialog wxCD(this);
    if (wxCD.ShowModal() == wxID_OK) {
        *currentColourPicked = wxCD.GetColourData().GetColour();
    }
    Refresh();
}

void Canvas::onTextInput(wxCommandEvent &event) {
    garageName = event.GetString();
    Refresh();
}

void Canvas::onChoice(wxCommandEvent &event) {
    currentCarPicked = event.GetSelection();
    Refresh();
}

void Canvas::onSaveFile(wxCommandEvent &event) {
    wxClientDC clientDC(this);
    int width, height;
    clientDC.GetSize(&width, &height);
    wxBitmap screenshot(width, height);
    wxMemoryDC memDC;
    memDC.SelectObject(screenshot);
    memDC.Blit(0, 0, width, height, &clientDC, 0, 0);
    memDC.SelectObject(wxNullBitmap);

    wxFileDialog saveFileDialog(this, _("Save file"), "", "",
                                "To BMP (*.bmp)|*.bmp|To PNG (*.png)|*.png|To TIFF (*.tiff)|*.tiff|TO JPEG (*.jpeg)|*.jpeg", wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
    if (saveFileDialog.ShowModal() == wxID_CANCEL) return;

    wxBitmapType bitmapType = wxBITMAP_TYPE_PNG;
    wxString pickedExtension(saveFileDialog.GetWildcard());
    std::cout << saveFileDialog.GetDirectory() << "  " << saveFileDialog.GetFilename() << std::endl;
    if(pickedExtension == "*.bmp") bitmapType = wxBITMAP_TYPE_BMP;
    if(pickedExtension == "*.png") bitmapType = wxBITMAP_TYPE_PNG;
    if(pickedExtension == "*.tiff") bitmapType = wxBITMAP_TYPE_TIFF;
    if(pickedExtension == "*.jpeg") bitmapType = wxBITMAP_TYPE_JPEG;

    screenshot.SaveFile(saveFileDialog.GetPath(), bitmapType);
}

