#include "MainFrame.h"


MainFrame::MainFrame(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &pos, const wxSize &size,
                     long style) : wxFrame(parent, id, title, pos, size, style) {
    this->SetSizeHints(wxSize(-1, -1), wxDefaultSize);



    wxBoxSizer *mainSizer = new wxBoxSizer(wxHORIZONTAL);

    wxBoxSizer *leftButtonSizer = new wxBoxSizer(wxVERTICAL);
    leftButtonSizer->SetMinSize(wxSize(150, -1));
    leftButtonSizer->Add(0, 0, 1, wxEXPAND, 5);

    wxBoxSizer *sliderSizer = new wxBoxSizer(wxHORIZONTAL);
    sliderSizer->Add(0, 0, 1, wxEXPAND, 5);
    carMovementSlider = new wxSlider(this, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL);
    sliderSizer->Add(carMovementSlider, 7, wxALIGN_CENTER_HORIZONTAL | wxALIGN_CENTER_VERTICAL | wxALL | wxEXPAND, 5);
    sliderSizer->Add(0, 0, 1, wxEXPAND, 5);
    leftButtonSizer->Add(sliderSizer, 0, wxEXPAND, 5);

    lanternCheckBox = new wxCheckBox(this, wxID_ANY, wxT("Włącz latarnię!"), wxDefaultPosition, wxDefaultSize, 0);
    leftButtonSizer->Add(lanternCheckBox, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

    lanternColorButton = new wxButton(this, wxID_ANY, wxT("Kolor Światła"), wxDefaultPosition, wxDefaultSize, 0);
    leftButtonSizer->Add(lanternColorButton, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

    garageNameTextCtrl = new wxTextCtrl(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, 0);
    leftButtonSizer->Add(garageNameTextCtrl, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

    wxString carNameChoiceArray[] = {wxT("AUDI"), wxT("BMW")};
    int carNameChoiceArraySize = sizeof(carNameChoiceArray) / sizeof(wxString);
    carNameChoice = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, carNameChoiceArraySize,
                                 carNameChoiceArray, 0);
    leftButtonSizer->Add(carNameChoice, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);

    saveToFileButton = new wxBitmapButton(this, wxID_ANY, wxBitmap(wxT("pictures/icon.png"), wxBITMAP_TYPE_ANY),
                                          wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW);
    leftButtonSizer->Add(saveToFileButton, 0, wxALL | wxALIGN_CENTER_HORIZONTAL, 5);
    leftButtonSizer->Add(0, 0, 1, wxEXPAND, 5);

    mainSizer->Add(leftButtonSizer, 0, wxALIGN_CENTER_VERTICAL | wxEXPAND, 5);


    wxBoxSizer *rightCanvasSizer;
    rightCanvasSizer = new wxBoxSizer(wxVERTICAL);

    rightCanvasSizer->Add(0, 0, 1, wxEXPAND, 5);
    canvas = new Canvas(this, wxID_ANY);

    rightCanvasSizer->Add(canvas, 0, wxALIGN_CENTER_HORIZONTAL, 5);
    rightCanvasSizer->Add(0, 0, 1, wxEXPAND, 5);

    mainSizer->Add(rightCanvasSizer, 1, wxALL | wxEXPAND, 5);

    this->SetSizer(mainSizer);
    this->Layout();
    this->Centre(wxBOTH);

    carMovementSlider->Bind(wxEVT_SCROLL_THUMBTRACK, &Canvas::onSliderMove, canvas);
    lanternCheckBox->Bind(wxEVT_CHECKBOX, &Canvas::onCheckboxChange, canvas);
    lanternColorButton->Bind(wxEVT_BUTTON, &Canvas::onLanternColorButtonClicked, canvas);
    garageNameTextCtrl->Bind(wxEVT_TEXT, &Canvas::onTextInput, canvas);
    carNameChoice->Bind(wxEVT_CHOICE, &Canvas::onChoice, canvas);
    saveToFileButton->Bind(wxEVT_BUTTON, &Canvas::onSaveFile, canvas);

    garageNameTextCtrl->SetValue("Siema");
    carNameChoice->SetSelection(0);
}



MainFrame::~MainFrame() {

}
