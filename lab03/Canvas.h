#pragma once

#include <wx/wx.h>

class Canvas : public wxPanel{
private:
    wxString garageName;
    wxImageHandler *pngHandler;
    wxImage *backgroundImage;
    wxImage *foregroundImage;
    wxPoint lantern[5];
    wxImage *cars[2];

    int currentCarPicked;
    wxPoint carPosition;
    bool isLanternOn;
    wxColour *currentColourPicked;

    void calculateCurrentCarPosition(int carSliderInput);
public:
    Canvas(wxWindow *parent,
    wxWindowID winid = wxID_ANY,
    const wxPoint& pos = wxDefaultPosition,
    const wxSize& size = wxSize(500, 500),
    long style = wxTAB_TRAVERSAL | wxNO_BORDER,
    const wxString& name = wxPanelNameStr);

    void onPaint(wxPaintEvent &event);
    void onSliderMove(wxScrollEvent &event);
    void onCheckboxChange(wxCommandEvent &event);
    void onLanternColorButtonClicked(wxCommandEvent &event);
    void onTextInput(wxCommandEvent &event);
    void onChoice(wxCommandEvent &event);
    void onSaveFile(wxCommandEvent &event);
};
