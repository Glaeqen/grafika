#pragma once
#include <stdio.h>

struct Vector4 {
    double data[4];

    Vector4();

    Vector4(double x, double y, double z);

    void Print() const;
};

struct Matrix4 {
    double data[4][4];

    Matrix4();

    void Print() const;

    Matrix4 operator*(const Matrix4) const;

    Vector4 operator*(const Vector4) const;
};

