//
// Created by glaeqen on 06/04/17.
//

#ifndef LAB04GRAFIKA_DRAWINGCONFIG_H
#define LAB04GRAFIKA_DRAWINGCONFIG_H


#include <vector>
#include <wx/wx.h>
#include "vecmat.h"

struct Line {
    Vector4 start;
    Vector4 end;
    wxColour colour;

    Line(const Vector4 &start, const Vector4 &end, const wxColour &colour):start(start), end(end), colour(colour){}
};

class DrawingConfig {
    double translationX;
    double translationY;
    double translationZ;

    double rotationX;
    double rotationY;
    double rotationZ;

    double scaleX;
    double scaleY;
    double scaleZ;

    std::vector<Line> lines;
public:
    DrawingConfig();

    double getTranslationX() const;

    void setTranslationX(double translationX);

    double getTranslationY() const;

    void setTranslationY(double translationY);

    double getTranslationZ() const;

    void setTranslationZ(double translationZ);

    double getRotationX() const;

    void setRotationX(double rotationX);

    double getRotationY() const;

    void setRotationY(double rotationY);

    double getRotationZ() const;

    void setRotationZ(double rotationZ);

    double getScaleX() const;

    void setScaleX(double scaleX);

    double getScaleY() const;

    void setScaleY(double scaleY);

    double getScaleZ() const;

    void setScaleZ(double scaleZ);

    std::vector<Line> &getLine();

    std::vector<Line> getLine() const;

};


#endif //LAB04GRAFIKA_DRAWINGCONFIG_H
