//---------------------------------------------------------------------------
//
// Name:        Lab05App.h
// Author:      Laptop_Admin
// Created:     2012-11-14 21:53:51
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __LAB05DLGApp_h__
#define __LAB05DLGApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class Lab05DlgApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
