//
// Created by glaeqen on 06/04/17.
//

#include "vecmat.h"

Vector4::Vector4() {
    data[0] = 0.0;
    data[1] = 0.0;
    data[2] = 0.0;
    data[3] = 1.0;
}

void Vector4::Print() const {
    printf("(%2.3lf,%2.3lf,%2.3lf,%2.3lf)\n", data[0], data[1], data[2], data[3]);
}

Vector4::Vector4(double x, double y, double z) {
    data[0] = x;
    data[1] = y;
    data[2] = z;
    data[3] = 1.0;
}

Matrix4::Matrix4() {
    data[0][0] = 1.0;
    data[0][1] = 0.0;
    data[0][2] = 0.0;
    data[0][3] = 0.0;
    data[1][0] = 0.0;
    data[1][1] = 1.0;
    data[1][2] = 0.0;
    data[1][3] = 0.0;
    data[2][0] = 0.0;
    data[2][1] = 0.0;
    data[2][2] = 1.0;
    data[2][3] = 0.0;
    data[3][0] = 0.0;
    data[3][1] = 0.0;
    data[3][2] = 0.0;
    data[3][3] = 1.0;
}

void Matrix4::Print() const {
    printf("\n|%2.3lf,%2.3lf,%2.3lf,%2.3lf|\n", data[0][0], data[0][1], data[0][2], data[0][3]);
    printf("|%2.3lf,%2.3lf,%2.3lf,%2.3lf|\n", data[1][0], data[1][1], data[1][2], data[1][3]);
    printf("|%2.3lf,%2.3lf,%2.3lf,%2.3lf|\n", data[2][0], data[2][1], data[2][2], data[2][3]);
    printf("|%2.3lf,%2.3lf,%2.3lf,%2.3lf|\n", data[3][0], data[3][1], data[3][2], data[2][3]);
}

Matrix4 Matrix4::operator*(const Matrix4 gMatrix) const {
    int i, j, k;
    Matrix4 tmp;

    for (i = 0; i < 4; i++)
        for (j = 0; j < 4; j++) {
            tmp.data[i][j] = 0.0;
            for (k = 0; k < 4; k++)
                tmp.data[i][j] = tmp.data[i][j] + (data[i][k] * gMatrix.data[k][j]);
        }
    return tmp;
}

Vector4 Matrix4::operator*(const Vector4 gVector) const {
    unsigned int i, j;
    Vector4 tmp;

    for (i = 0; i < 4; i++) {
        tmp.data[i] = 0.0;
        for (j = 0; j < 4; j++) tmp.data[i] = tmp.data[i] + (data[i][j] * gVector.data[j]);
    }
    return tmp;
}
