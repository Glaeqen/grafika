//---------------------------------------------------------------------------
//
// Name:        Lab05App.cpp
// Author:      Laptop_Admin
// Created:     2012-11-14 21:53:51
// Description: 
//
//---------------------------------------------------------------------------

#include "Lab05App.h"
#include "Lab05Dlg.h"

IMPLEMENT_APP(Lab05DlgApp)

bool Lab05DlgApp::OnInit()
{
	Lab05Dlg* dialog = new Lab05Dlg(NULL);
	SetTopWindow(dialog);
	dialog->Show(true);		
	return true;
}
 
int Lab05DlgApp::OnExit()
{
	return 0;
}
