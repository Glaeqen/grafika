
//
// Created by glaeqen on 06/04/17.
//

#include "Drawing.h"

Drawing::Drawing(wxSize screenSize, const DrawingConfig *cfg) : cfg(cfg), projectorScreenPosZ(PROJECTORSCREENPOSZ),
                                                                observatorPosZ(OBSERVATORPOSZ),
                                                                screenMaxX((fabs(projectorScreenPosZ) +
                                                                            fabs(observatorPosZ)) /
                                                                           fabs(observatorPosZ)),
                                                                screenMaxY(screenMaxX),
                                                                screenMinX(-screenMaxX),
                                                                screenMinY(-screenMaxX),
                                                                screenSize(screenSize) {

}

Matrix4 Drawing::getTranslation() const {
    Matrix4 matrix;
    matrix.data[0][3] = cfg->getTranslationX();
    matrix.data[1][3] = cfg->getTranslationY();
    matrix.data[2][3] = cfg->getTranslationZ();
    return matrix;
}

Matrix4 Drawing::getRotationX() const {
    Matrix4 matrix;
    matrix.data[1][1] = cos(cfg->getRotationX() * M_PI / 180);
    matrix.data[1][2] = -sin(cfg->getRotationX() * M_PI / 180);
    matrix.data[2][1] = sin(cfg->getRotationX() * M_PI / 180);
    matrix.data[2][2] = cos(cfg->getRotationX() * M_PI / 180);
    return matrix;
}

Matrix4 Drawing::getRotationY() const {
    Matrix4 matrix;
    matrix.data[0][0] = cos(cfg->getRotationY() * M_PI / 180);
    matrix.data[2][0] = -sin(cfg->getRotationY() * M_PI / 180);
    matrix.data[0][2] = sin(cfg->getRotationY() * M_PI / 180);
    matrix.data[2][2] = cos(cfg->getRotationY() * M_PI / 180);
    return matrix;
}

Matrix4 Drawing::getRotationZ() const {
    Matrix4 matrix;
    matrix.data[0][0] = cos(cfg->getRotationZ() * M_PI / 180);
    matrix.data[0][1] = -sin(cfg->getRotationZ() * M_PI / 180);
    matrix.data[1][0] = sin(cfg->getRotationZ() * M_PI / 180);
    matrix.data[1][1] = cos(cfg->getRotationZ() * M_PI / 180);
    return matrix;
}

Matrix4 Drawing::getScale() const {
    Matrix4 matrix;
    matrix.data[0][0] = cfg->getScaleX();
    matrix.data[1][1] = cfg->getScaleY();
    matrix.data[2][2] = cfg->getScaleZ();
    return matrix;
}

void Drawing::transformLine3D(const Matrix4 &transformation, Line &line) const {
    line.start = transformation * line.start;
    line.end = transformation * line.end;
}

wxPoint Drawing::toScreenPoint(const wxRealPoint &realPoint) const {

    wxPoint pointOnScreen;
    pointOnScreen.x = static_cast<int>((realPoint.x - screenMinX) * screenSize.x / (screenMaxX - screenMinX));
    pointOnScreen.y = static_cast<int>((1 - (realPoint.y - screenMinY) / (screenMaxY - screenMinY)) *
                                       screenSize.y);
    return pointOnScreen;
}

void Drawing::putLineOnScreen(wxDC *dc, const Line &line) const {
    double screenObservatorDistance = fabs(projectorScreenPosZ - observatorPosZ);
    wxRealPoint start, end;
    start.x = screenObservatorDistance * line.start.data[0] / (line.start.data[2] + fabs(observatorPosZ) + 1);
    start.y = screenObservatorDistance * line.start.data[1] / (line.start.data[2] + fabs(observatorPosZ) + 1);
    end.x = screenObservatorDistance * line.end.data[0] / (line.end.data[2] + fabs(observatorPosZ) + 1);
    end.y = screenObservatorDistance * line.end.data[1] / (line.end.data[2] + fabs(observatorPosZ) + 1);
    dc->SetPen(wxPen(line.colour));
    wxPoint startOnScreen = toScreenPoint(start);
    wxPoint endOnScreen = toScreenPoint(end);
    dc->DrawLine(startOnScreen, endOnScreen);
}

void Drawing::draw(wxDC *dc) const {
    std::vector<Line> linesToDraw = cfg->getLine();
    for (int i = 0; i < linesToDraw.size(); i++) {
        transformLine3D(getScale(), linesToDraw[i]);
        transformLine3D(getRotationX(), linesToDraw[i]);
        transformLine3D(getRotationY(), linesToDraw[i]);
        transformLine3D(getRotationZ(), linesToDraw[i]);
        transformLine3D(getTranslation(), linesToDraw[i]);
        putLineOnScreen(dc, linesToDraw[i]);
    }
}

