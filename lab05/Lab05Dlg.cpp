//---------------------------------------------------------------------------
//
// Name:        Lab05Dlg.cpp
// Author:      Janusz Malinowski
// Created:     2008-10-02 16:16:34
// Description: Lab05Dlg class implementation
//
//---------------------------------------------------------------------------

#include "Lab05Dlg.h"
#include "Drawing.h"

#include <wx/dcbuffer.h>
#include <fstream>

////Event Table Start
BEGIN_EVENT_TABLE(Lab05Dlg, wxDialog)
                ////Manual Code Start
                ////Manual Code End

                EVT_CLOSE(Lab05Dlg::OnClose)

                EVT_COMMAND_SCROLL(ID_WXSB_SCALEZ, Lab05Dlg::WxSB_ScaleZScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_SCALEY, Lab05Dlg::WxSB_ScaleYScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_SCALEX, Lab05Dlg::WxSB_ScaleXScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_ROTATEZ, Lab05Dlg::WxSB_RotateZScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_ROTATEY, Lab05Dlg::WxSB_RotateYScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_ROTATEX, Lab05Dlg::WxSB_RotateXScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_TRANSLATIONZ, Lab05Dlg::WxSB_TranslationZScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_TRANSLATIONY, Lab05Dlg::WxSB_TranslationYScroll)

                EVT_COMMAND_SCROLL(ID_WXSB_TRANSLATIONX, Lab05Dlg::WxSB_TranslationXScroll)

                EVT_BUTTON(ID_WXBUTTONLOAD, Lab05Dlg::WxButtonLoadClick)

                EVT_UPDATE_UI(ID_WXPANEL, Lab05Dlg::WxPanelUpdateUI)
END_EVENT_TABLE()
////Event Table End

Lab05Dlg::Lab05Dlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize &size,
                   long style)
        : wxDialog(parent, id, title, position, size, style) {
    cfg = new DrawingConfig();
    CreateGUIControls();
}

Lab05Dlg::~Lab05Dlg() {
}

void Lab05Dlg::CreateGUIControls() {
    //Do not add custom code between
    //GUI Items Creation Start and GUI Items Creation End.
    //wxDev-C++ designer will remove them.
    //Add the custom code before or after the blocks
    ////GUI Items Creation Start

    WxBoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
    this->SetSizer(WxBoxSizer1);
    this->SetAutoLayout(true);

    WxPanel = new wxPanel(this, ID_WXPANEL, wxPoint(5, 168), wxSize(600, 600));
    WxPanel->SetMinSize(wxSize(600, 600));
    WxPanel->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer1->Add(WxPanel, 1, wxEXPAND | wxALL, 5);

    WxBoxSizer2 = new wxBoxSizer(wxVERTICAL);
    WxBoxSizer1->Add(WxBoxSizer2, 0, wxALIGN_CENTER | wxALL, 5);

    WxButtonLoad = new wxButton(this, ID_WXBUTTONLOAD, wxT("Wczytaj Geometrię"), wxPoint(67, 5), wxSize(114, 25), 0,
                                wxDefaultValidator, wxT("WxButtonLoad"));
    WxButtonLoad->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer2->Add(WxButtonLoad, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer6 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer6, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText4 = new wxStaticText(this, ID_WXSTATICTEXT4, wxT("Translacja X:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText4"));
    WxStaticText4->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxStaticText4, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_TranslationX = new wxScrollBar(this, ID_WXSB_TRANSLATIONX, wxPoint(81, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                        wxDefaultValidator, wxT("WxSB_TranslationX"));
    WxSB_TranslationX->Enable(false);
    WxSB_TranslationX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxSB_TranslationX, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_TranslationX = new wxStaticText(this, ID_WXST_TRANSLATIONX, wxT("0    "), wxPoint(212, 5), wxDefaultSize, 0,
                                         wxT("WxST_TranslationX"));
    WxST_TranslationX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer6->Add(WxST_TranslationX, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer7, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText5 = new wxStaticText(this, ID_WXSTATICTEXT5, wxT("Translacja Y:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText5"));
    WxStaticText5->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxStaticText5, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_TranslationY = new wxScrollBar(this, ID_WXSB_TRANSLATIONY, wxPoint(81, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                        wxDefaultValidator, wxT("WxSB_TranslationY"));
    WxSB_TranslationY->Enable(false);
    WxSB_TranslationY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxSB_TranslationY, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_TranslationY = new wxStaticText(this, ID_WXST_TRANSLATIONY, wxT("0    "), wxPoint(212, 5), wxDefaultSize, 0,
                                         wxT("WxST_TranslationY"));
    WxST_TranslationY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer7->Add(WxST_TranslationY, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer8 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer8, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText6 = new wxStaticText(this, ID_WXSTATICTEXT6, wxT("Translacja Z:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText6"));
    WxStaticText6->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(WxStaticText6, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_TranslationZ = new wxScrollBar(this, ID_WXSB_TRANSLATIONZ, wxPoint(81, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                        wxDefaultValidator, wxT("WxSB_TranslationZ"));
    WxSB_TranslationZ->Enable(false);
    WxSB_TranslationZ->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(WxSB_TranslationZ, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_TranslationZ = new wxStaticText(this, ID_WXST_TRANSLATIONZ, wxT("20   "), wxPoint(212, 5), wxDefaultSize, 0,
                                         wxT("WxST_TranslationZ"));
    WxST_TranslationZ->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer8->Add(WxST_TranslationZ, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer3, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText1 = new wxStaticText(this, ID_WXSTATICTEXT1, wxT("Obrót X:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText1"));
    WxStaticText1->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxStaticText1, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_RotateX = new wxScrollBar(this, ID_WXSB_ROTATEX, wxPoint(60, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                   wxDefaultValidator, wxT("WxSB_RotateX"));
    WxSB_RotateX->Enable(false);
    WxSB_RotateX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxSB_RotateX, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_RotateX = new wxStaticText(this, ID_WXST_ROTATEX, wxT("0  "), wxPoint(191, 5), wxDefaultSize, 0,
                                    wxT("WxST_RotateX"));
    WxST_RotateX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer3->Add(WxST_RotateX, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer4, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText2 = new wxStaticText(this, ID_WXSTATICTEXT2, wxT("Obrót Y:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText2"));
    WxStaticText2->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxStaticText2, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_RotateY = new wxScrollBar(this, ID_WXSB_ROTATEY, wxPoint(60, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                   wxDefaultValidator, wxT("WxSB_RotateY"));
    WxSB_RotateY->Enable(false);
    WxSB_RotateY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxSB_RotateY, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_RotateY = new wxStaticText(this, ID_WXST_ROTATEY, wxT("0  "), wxPoint(191, 5), wxDefaultSize, 0,
                                    wxT("WxST_RotateY"));
    WxST_RotateY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer4->Add(WxST_RotateY, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer5 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer5, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText3 = new wxStaticText(this, ID_WXSTATICTEXT3, wxT("Obrót Z:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText3"));
    WxStaticText3->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer5->Add(WxStaticText3, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_RotateZ = new wxScrollBar(this, ID_WXSB_ROTATEZ, wxPoint(60, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                   wxDefaultValidator, wxT("WxSB_RotateZ"));
    WxSB_RotateZ->Enable(false);
    WxSB_RotateZ->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer5->Add(WxSB_RotateZ, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_RotateZ = new wxStaticText(this, ID_WXST_ROTATEZ, wxT("0  "), wxPoint(191, 5), wxDefaultSize, 0,
                                    wxT("WxST_RotateZ"));
    WxST_RotateZ->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer5->Add(WxST_RotateZ, 0, wxALIGN_CENTER | wxALL, 5);

    WxOpenFileDialog = new wxFileDialog(this, wxT("Choose a file"), wxT(""), wxT(""), wxT("*.*"), wxFD_OPEN);

    WxBoxSizer9 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer9, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer10 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer10, 0, wxALIGN_CENTER | wxALL, 5);

    WxBoxSizer11 = new wxBoxSizer(wxHORIZONTAL);
    WxBoxSizer2->Add(WxBoxSizer11, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText7 = new wxStaticText(this, ID_WXSTATICTEXT7, wxT("Skala X:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText7"));
    WxStaticText7->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer9->Add(WxStaticText7, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText8 = new wxStaticText(this, ID_WXSTATICTEXT8, wxT("Skala Y:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText8"));
    WxStaticText8->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer10->Add(WxStaticText8, 0, wxALIGN_CENTER | wxALL, 5);

    WxStaticText9 = new wxStaticText(this, ID_WXSTATICTEXT9, wxT("Skala Z:"), wxPoint(5, 5), wxDefaultSize, 0,
                                     wxT("WxStaticText9"));
    WxStaticText9->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer11->Add(WxStaticText9, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_ScaleX = new wxScrollBar(this, ID_WXSB_SCALEX, wxPoint(57, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                  wxDefaultValidator, wxT("WxSB_ScaleX"));
    WxSB_ScaleX->Enable(false);
    WxSB_ScaleX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer9->Add(WxSB_ScaleX, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_ScaleY = new wxScrollBar(this, ID_WXSB_SCALEY, wxPoint(57, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                  wxDefaultValidator, wxT("WxSB_ScaleY"));
    WxSB_ScaleY->Enable(false);
    WxSB_ScaleY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer10->Add(WxSB_ScaleY, 0, wxALIGN_CENTER | wxALL, 5);

    WxSB_ScaleZ = new wxScrollBar(this, ID_WXSB_SCALEZ, wxPoint(57, 5), wxSize(121, 16), wxSB_HORIZONTAL,
                                  wxDefaultValidator, wxT("WxSB_ScaleZ"));
    WxSB_ScaleZ->Enable(false);
    WxSB_ScaleZ->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer11->Add(WxSB_ScaleZ, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_ScaleX = new wxStaticText(this, ID_WXST_SCALEX, wxT("1.0  "), wxPoint(188, 5), wxDefaultSize, 0,
                                   wxT("WxST_ScaleX"));
    WxST_ScaleX->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer9->Add(WxST_ScaleX, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_ScaleY = new wxStaticText(this, ID_WXST_SCALEY, wxT("1.0  "), wxPoint(188, 5), wxDefaultSize, 0,
                                   wxT("WxST_ScaleY"));
    WxST_ScaleY->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer10->Add(WxST_ScaleY, 0, wxALIGN_CENTER | wxALL, 5);

    WxST_ScaleZ = new wxStaticText(this, ID_WXST_SCALEZ, wxT("1.0  "), wxPoint(188, 5), wxDefaultSize, 0,
                                   wxT("WxST_ScaleZ"));
    WxST_ScaleZ->SetFont(wxFont(8, wxSWISS, wxNORMAL, wxNORMAL, false, wxT("Tahoma")));
    WxBoxSizer11->Add(WxST_ScaleZ, 0, wxALIGN_CENTER | wxALL, 5);

    SetTitle(wxT("Lab05"));
    SetIcon(wxNullIcon);

    GetSizer()->Layout();
    GetSizer()->Fit(this);
    GetSizer()->SetSizeHints(this);
    Center();

    ////GUI Items Creation End
    WxOpenFileDialog->SetWildcard("Geometry file (*.geo)|*.geo");

    WxSB_TranslationX->SetScrollbar(100, 1, 201, 1, true);
    WxSB_TranslationX->Enable(true);
    WxSB_TranslationY->SetScrollbar(100, 1, 201, 1, true);
    WxSB_TranslationY->Enable(true);
    WxSB_TranslationZ->SetScrollbar(200, 1, 401, 1, true);
    WxSB_TranslationZ->Enable(true);

    WxSB_RotateX->SetScrollbar(0, 1, 361, 1, true);
    WxSB_RotateX->Enable(true);
    WxSB_RotateY->SetScrollbar(0, 1, 361, 1, true);
    WxSB_RotateY->Enable(true);
    WxSB_RotateZ->SetScrollbar(0, 1, 361, 1, true);
    WxSB_RotateZ->Enable(true);

    WxSB_ScaleX->SetScrollbar(99, 1, 200, 1, true);
    WxSB_ScaleX->Enable(true);
    WxSB_ScaleY->SetScrollbar(99, 1, 200, 1, true);
    WxSB_ScaleY->Enable(true);
    WxSB_ScaleZ->SetScrollbar(99, 1, 200, 1, true);
    WxSB_ScaleZ->Enable(true);
}

void Lab05Dlg::OnClose(wxCloseEvent & /*event*/) {
    Destroy();
}

/*
 * WxButtonLoadClick
 */
void Lab05Dlg::WxButtonLoadClick(wxCommandEvent &event) {
    if (WxOpenFileDialog->ShowModal() == wxID_OK) {
        double x1, y1, z1, x2, y2, z2;
        int r, g, b;

        std::string line;
        std::ifstream in(WxOpenFileDialog->GetPath().ToAscii());
        if (in.is_open()) {
            cfg->getLine().clear();
            while (!in.eof()) {
                in >> x1 >> y1 >> z1 >> x2 >> y2 >> z2 >> r >> g >> b;
                cfg->getLine().push_back(Line(Vector4(x1, y1, z1), Vector4(x2, y2, z2),
                                              wxColour((unsigned char) r, (unsigned char) g, (unsigned char) b)));
            }
            in.close();
        }
    }
}

void Lab05Dlg::Repaint() {
    wxClientDC dc1(WxPanel);
    wxBufferedDC dc(&dc1, WxPanel->GetSize());

    dc.SetBackground(wxBrush(wxColour(255, 255, 255)));
    dc.Clear();
    dc.SetClippingRegion(wxRect(0, 0, WxPanel->GetSize().x, WxPanel->GetSize().y));
    Drawing drawing(WxPanel->GetSize(), cfg);
    drawing.draw(&dc);
}

/*
 * WxPanelUpdateUI
 */
void Lab05Dlg::WxPanelUpdateUI(wxUpdateUIEvent &event) {
    Repaint();
}

/*
 * WxSB_TranslacjaXScroll
 */
void Lab05Dlg::WxSB_TranslationXScroll(wxScrollEvent &event) {
    wxString str;
    double param = WxSB_TranslationX->GetThumbPosition() / 5.0 - 20.0;
    cfg->setTranslationX(param);
    str << param;
    WxST_TranslationX->SetLabel(str);
    Repaint();
}

/*
 * WxSB_TranslationYScroll
 */
void Lab05Dlg::WxSB_TranslationYScroll(wxScrollEvent &event) {
    wxString str;
    double param = WxSB_TranslationY->GetThumbPosition() / 5.0 - 20.0;
    cfg->setTranslationY(param);
    str << param;
    WxST_TranslationY->SetLabel(str);
    Repaint();
}

/*
 * WxSB_TranslationZScroll
 */
void Lab05Dlg::WxSB_TranslationZScroll(wxScrollEvent &event) {
    wxString str;
    double param = WxSB_TranslationZ->GetThumbPosition() / 5.0 - 20.0;
    cfg->setTranslationZ(param);
    str << param;
    WxST_TranslationZ->SetLabel(str);
    Repaint();
}

/*
 * WxSB_RotateXScroll
 */
void Lab05Dlg::WxSB_RotateXScroll(wxScrollEvent &event) {
    wxString str;
    double param = (WxSB_RotateX->GetThumbPosition());
    cfg->setRotationX(param);
    str << param;
    WxST_RotateX->SetLabel(str);
    Repaint();
}

/*
 * WxSB_RotateYScroll
 */
void Lab05Dlg::WxSB_RotateYScroll(wxScrollEvent &event) {
    wxString str;
    double param = (WxSB_RotateY->GetThumbPosition());
    cfg->setRotationY(param);
    str << param;
    WxST_RotateY->SetLabel(str);
    Repaint();
}

/*
 * WxSB_RotateZScroll
 */
void Lab05Dlg::WxSB_RotateZScroll(wxScrollEvent &event) {
    wxString str;
    double param = (WxSB_RotateZ->GetThumbPosition());
    cfg->setRotationZ(param);
    str << param;
    WxST_RotateZ->SetLabel(str);
    Repaint();
}

/*
 * WxSB_ScaleXScroll
 */
void Lab05Dlg::WxSB_ScaleXScroll(wxScrollEvent &event) {
    wxString str;
    double param = (WxSB_ScaleX->GetThumbPosition() + 1.0) / 100.;
    cfg->setScaleX(param);
    str << (WxSB_ScaleX->GetThumbPosition() + 1.0) / 100.0;
    WxST_ScaleX->SetLabel(str);
    Repaint();
}

/*
 * WxSB_ScaleYScroll
 */
void Lab05Dlg::WxSB_ScaleYScroll(wxScrollEvent &event) {
    wxString str;
    double param = (WxSB_ScaleY->GetThumbPosition() + 1.0) / 100.0;
    cfg->setScaleY(param);
    str << param;
    WxST_ScaleY->SetLabel(str);
    Repaint();
}

/*
 * WxSB_ScaleZScroll
 */
void Lab05Dlg::WxSB_ScaleZScroll(wxScrollEvent &event) {
    wxString str;
    double param = (WxSB_ScaleZ->GetThumbPosition() + 1.0) / 100.0;
    cfg->setScaleZ(param);
    str << param;
    WxST_ScaleZ->SetLabel(str);
    Repaint();
}
