//
// Created by glaeqen on 06/04/17.
//

#ifndef LAB04GRAFIKA_DRAWING_H
#define LAB04GRAFIKA_DRAWING_H


#include <wx/wx.h>
#include "vecmat.h"
#include "DrawingConfig.h"

#define PROJECTORSCREENPOSZ 60
#define OBSERVATORPOSZ -30

class Drawing {
    const DrawingConfig *cfg;
    const double projectorScreenPosZ, observatorPosZ;
    const double screenMaxX, screenMaxY, screenMinX, screenMinY;
    wxSize screenSize;

    Matrix4 getTranslation() const;

    Matrix4 getRotationX() const;

    Matrix4 getRotationY() const;

    Matrix4 getRotationZ() const;

    Matrix4 getScale() const;

    void transformLine3D(const Matrix4 &transformation, Line &line) const;

    wxPoint toScreenPoint(const wxRealPoint &realPoint) const;

    void putLineOnScreen(wxDC *dc, const Line &line) const;

public:
    Drawing(wxSize screenSize, const DrawingConfig *cfg);

    void draw(wxDC *dc) const;

};


#endif //LAB04GRAFIKA_DRAWING_H
