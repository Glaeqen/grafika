//---------------------------------------------------------------------------
//
// Name:        Lab05Dlg.h
// Author:      Janusz Malinowski
// Created:     2008-10-02 16:16:34
// Description: Lab05Dlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __LAB05DLG_h__
#define __LAB05DLG_h__

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP

#include <wx/wx.h>
#include <wx/dialog.h>

#else
#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/filedlg.h>
#include <wx/scrolbar.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include "DrawingConfig.h"
////Header Include End

////Dialog Style Start
#undef Lab05Dlg_STYLE
#define Lab05Dlg_STYLE wxCAPTION | wxRESIZE_BORDER | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class Lab05Dlg : public wxDialog {
private:
DECLARE_EVENT_TABLE()

public:
    Lab05Dlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Lab05"),
             const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize, long style = Lab05Dlg_STYLE);

    virtual ~Lab05Dlg();

    void WxButtonLoadClick(wxCommandEvent &event);

    void Repaint();

    void WxPanelUpdateUI(wxUpdateUIEvent &event);

    void WxSB_TranslationXScroll(wxScrollEvent &event);

    void WxSB_TranslationYScroll(wxScrollEvent &event);

    void WxSB_TranslationZScroll(wxScrollEvent &event);

    void WxSB_RotateXScroll(wxScrollEvent &event);

    void WxSB_RotateYScroll(wxScrollEvent &event);

    void WxSB_RotateZScroll(wxScrollEvent &event);

    void WxSB_ScaleXScroll(wxScrollEvent &event);

    void WxSB_ScaleYScroll(wxScrollEvent &event);

    void WxSB_ScaleZScroll(wxScrollEvent &event);

private:
    //Do not add custom control declarations between
    //GUI Control Declaration Start and GUI Control Declaration End.
    //wxDev-C++ will remove them. Add custom code after the block.
    ////GUI Control Declaration Start
    wxStaticText *WxST_ScaleZ;
    wxStaticText *WxST_ScaleY;
    wxStaticText *WxST_ScaleX;
    wxScrollBar *WxSB_ScaleZ;
    wxScrollBar *WxSB_ScaleY;
    wxScrollBar *WxSB_ScaleX;
    wxStaticText *WxStaticText9;
    wxStaticText *WxStaticText8;
    wxStaticText *WxStaticText7;
    wxBoxSizer *WxBoxSizer11;
    wxBoxSizer *WxBoxSizer10;
    wxBoxSizer *WxBoxSizer9;
    wxFileDialog *WxOpenFileDialog;
    wxStaticText *WxST_RotateZ;
    wxScrollBar *WxSB_RotateZ;
    wxStaticText *WxStaticText3;
    wxBoxSizer *WxBoxSizer5;
    wxStaticText *WxST_RotateY;
    wxScrollBar *WxSB_RotateY;
    wxStaticText *WxStaticText2;
    wxBoxSizer *WxBoxSizer4;
    wxStaticText *WxST_RotateX;
    wxScrollBar *WxSB_RotateX;
    wxStaticText *WxStaticText1;
    wxBoxSizer *WxBoxSizer3;
    wxStaticText *WxST_TranslationZ;
    wxScrollBar *WxSB_TranslationZ;
    wxStaticText *WxStaticText6;
    wxBoxSizer *WxBoxSizer8;
    wxStaticText *WxST_TranslationY;
    wxScrollBar *WxSB_TranslationY;
    wxStaticText *WxStaticText5;
    wxBoxSizer *WxBoxSizer7;
    wxStaticText *WxST_TranslationX;
    wxScrollBar *WxSB_TranslationX;
    wxStaticText *WxStaticText4;
    wxBoxSizer *WxBoxSizer6;
    wxButton *WxButtonLoad;
    wxBoxSizer *WxBoxSizer2;
    wxPanel *WxPanel;
    wxBoxSizer *WxBoxSizer1;
    ////GUI Control Declaration End

private:
    //Note: if you receive any error with these enum IDs, then you need to
    //change your old form code that are based on the #define control IDs.
    //#defines may replace a numeric value for the enum names.
    //Try copy and pasting the below block in your old form header files.
    enum {
        ////GUI Enum Control ID Start
        ID_WXST_SCALEZ = 1044,
        ID_WXST_SCALEY = 1043,
        ID_WXST_SCALEX = 1042,
        ID_WXSB_SCALEZ = 1041,
        ID_WXSB_SCALEY = 1040,
        ID_WXSB_SCALEX = 1039,
        ID_WXSTATICTEXT9 = 1038,
        ID_WXSTATICTEXT8 = 1037,
        ID_WXSTATICTEXT7 = 1036,
        ID_WXST_ROTATEZ = 1032,
        ID_WXSB_ROTATEZ = 1015,
        ID_WXSTATICTEXT3 = 1013,
        ID_WXST_ROTATEY = 1031,
        ID_WXSB_ROTATEY = 1014,
        ID_WXSTATICTEXT2 = 1012,
        ID_WXST_ROTATEX = 1030,
        ID_WXSB_ROTATEX = 1009,
        ID_WXSTATICTEXT1 = 1008,
        ID_WXST_TRANSLATIONZ = 1029,
        ID_WXSB_TRANSLATIONZ = 1027,
        ID_WXSTATICTEXT6 = 1025,
        ID_WXST_TRANSLATIONY = 1028,
        ID_WXSB_TRANSLATIONY = 1026,
        ID_WXSTATICTEXT5 = 1024,
        ID_WXST_TRANSLATIONX = 1021,
        ID_WXSB_TRANSLATIONX = 1020,
        ID_WXSTATICTEXT4 = 1019,
        ID_WXBUTTONLOAD = 1017,
        ID_WXPANEL = 1002,
        ////GUI Enum Control ID End
                ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
    };

private:
    DrawingConfig *cfg;

    void OnClose(wxCloseEvent &event);

    void CreateGUIControls();
};

#endif
