//
// Created by glaeqen on 06/04/17.
//

#include "DrawingConfig.h"

DrawingConfig::DrawingConfig() {
    translationX = 0.0;
    translationY = 0.0;
    translationZ = 20.0;

    rotationX = 0.0;
    rotationY = 0.0;
    rotationZ = 0.0;

    scaleX = 1.0;
    scaleY = 1.0;
    scaleZ = 1.0;
}

double DrawingConfig::getTranslationX() const {
    return translationX;
}

void DrawingConfig::setTranslationX(double translationX) {
    DrawingConfig::translationX = translationX;
}

double DrawingConfig::getTranslationY() const {
    return translationY;
}

void DrawingConfig::setTranslationY(double translationY) {
    DrawingConfig::translationY = translationY;
}

double DrawingConfig::getTranslationZ() const {
    return translationZ;
}

void DrawingConfig::setTranslationZ(double translationZ) {
    DrawingConfig::translationZ = translationZ;
}

double DrawingConfig::getRotationX() const {
    return rotationX;
}

void DrawingConfig::setRotationX(double rotationX) {
    DrawingConfig::rotationX = rotationX;
}

double DrawingConfig::getRotationY() const {
    return rotationY;
}

void DrawingConfig::setRotationY(double rotationY) {
    DrawingConfig::rotationY = rotationY;
}

double DrawingConfig::getRotationZ() const {
    return rotationZ;
}

void DrawingConfig::setRotationZ(double rotationZ) {
    DrawingConfig::rotationZ = rotationZ;
}

double DrawingConfig::getScaleX() const {
    return scaleX;
}

void DrawingConfig::setScaleX(double scaleX) {
    DrawingConfig::scaleX = scaleX;
}

double DrawingConfig::getScaleY() const {
    return scaleY;
}

void DrawingConfig::setScaleY(double scaleY) {
    DrawingConfig::scaleY = scaleY;
}

double DrawingConfig::getScaleZ() const {
    return scaleZ;
}

void DrawingConfig::setScaleZ(double scaleZ) {
    DrawingConfig::scaleZ = scaleZ;
}

std::vector<Line> &DrawingConfig::getLine() {
    return lines;
}

std::vector<Line> DrawingConfig::getLine() const {
    return lines;
}

